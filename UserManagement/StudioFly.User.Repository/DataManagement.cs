﻿using Microsoft.EntityFrameworkCore;
using StudioFly.Users.EntityFramework;

namespace StudioFly.Users.Repository
{
    /// <summary>
    /// Manage the creation of db context
    /// </summary>
    public class DataManagement
    {
        /// <summary>
        /// Define the property of the db context
        /// </summary>
        public StudioFlyUserContext StudioFlyUserContext { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagement"/> class.
        /// </summary>
        public DataManagement()
        {
            StudioFlyUserContext = new StudioFlyContextFactory().CreateDbContext();
        }
        /// <summary>
        /// Commit data
        /// </summary>
        public void SaveData()
        {
            StudioFlyUserContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Employees ON");
            StudioFlyUserContext.SaveChanges();
            StudioFlyUserContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Employees OFF");
        }

        /// <summary>
        /// Disposes the studio fly context
        /// </summary>
        ~DataManagement()
        {
            StudioFlyUserContext.Dispose();
        }
    }
}
