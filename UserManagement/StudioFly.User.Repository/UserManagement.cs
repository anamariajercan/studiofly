﻿using StudioFly.Users.EntityFramework;
using StudioFly.Users.EntityFramework.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StudioFly.Users.Repository
{
    /// <summary>
    /// </summary>
    public class UserManagement : DataManagement, IUserManagement
    {
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public User GetUser(int id)
        {
            return StudioFlyUserContext.Users.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public User GetUser(User userModel)
        {
            return StudioFlyUserContext.Users.FirstOrDefault(x => x.Username == userModel.Username && x.Password == userModel.Password);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public User GetUser(string userName)
        {
            return StudioFlyUserContext.Users.FirstOrDefault(x => x.Username == userName);
        }

        /// <summary>
        /// Gets the client based on the provided id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public Client GetClient(int clientId)
        {
            StudioFlyUserContext = new StudioFlyContextFactory().CreateDbContext();
            return StudioFlyUserContext.Clients.FirstOrDefault(client => client.Id == clientId);
        }

        /// <summary>
        /// Gets the employee based on the provided id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Employee GetEmployee(int id)
        {
            return StudioFlyUserContext.Employees.FirstOrDefault(employee => employee.UserId == id);
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAllUsers()
        {
            return StudioFlyUserContext.Users;
        }
        public IEnumerable<Employee> GetAllEmployees()
        {
            var users = GetAllUsers();
            var employees = new List<Employee>();
            foreach (var user in users)
            {
                var employee = GetEmployee(user.Id);
                if (employee == null) continue;
                if (user.Id == employee.UserId)
                {
                    var employeePortfolio = GetEmployeePortfolio(employee.Id);
                    var description = UpdateEmployeeDescription(employee);
                    employee.EmployeePortfolio = employeePortfolio;
                    employee.User.Description = description;
                    employees.Add(employee);
                }
            }
            return employees;
        }

        private List<EmployeePortfolio> GetEmployeePortfolio(int employeeId)
        {
            var employeePortfolio = new List<EmployeePortfolio>();
            try
            {
                var employeesPortfolio = GetEmployeesPortfolio();
                employeePortfolio = employeesPortfolio.Where(x => x.EmployeeId == employeeId).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return employeePortfolio;
        }

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="user">The user model.</param>
        /// <returns></returns>
        public User AddUser(User user)
        {
            try
            {
                user.Password = SecurePassword(user.Password,user.Username);
                StudioFlyUserContext.Users.Add(user);
                SaveData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return GetUser(user.Id);
        }

        private static string SecurePassword(string password, string userName)
        {
            string securedPassword;
            using (var sha = System.Security.Cryptography.SHA512.Create())
            {
                var bytes = System.Text.Encoding.UTF8.GetBytes($"{password}{userName}");
                var hash = sha.ComputeHash(bytes);

              securedPassword = Convert.ToBase64String(hash);
            }
            return securedPassword;
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="newUser">The new user model.</param>
        /// <returns></returns>
        public User UpdateUser(User newUser)
        {
            var user = GetUser(newUser.Id);
            if (user == null)
            {
                return null;
            }

            StudioFlyUserContext.Users.Update(newUser);
            SaveData();
            return GetUser(user.Id);
        }
        /// <inheritdoc />
        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool DeleteUser(int id)
        {
            try
            {
                var user = GetUser(id);
                StudioFlyUserContext.Users.Remove(user);
                SaveData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public IEnumerable<Client> GetAllClients()
        {
            IEnumerable<Client> clients = StudioFlyUserContext.Clients;
            return clients;
        }

        public IEnumerable<EmployeePortfolio> GetEmployeesPortfolio()
        {
            IEnumerable<EmployeePortfolio> employeesPortfolios = StudioFlyUserContext.EmployeePortfolios;
            return employeesPortfolios;
        }

        public Client CreateClient(CreateClientModel clientModel)
        {
            var savedClient = new Client();
            var user = GetUser(clientModel.UserId);
            var client = ToClientModel(clientModel);
            if (user != null) return savedClient;
            var userModel = AddUser(client.User);
            client.User = userModel;
            savedClient = StudioFlyUserContext.Clients.Add(client).Entity;
            SaveData();
            return savedClient;
        }

        public bool DeleteClient(int clientId)
        {
            try
            {
                var client = GetClient(clientId);
                StudioFlyUserContext.Clients.Remove(client);
                SaveData();
                DeleteUser(client.UserId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        private Client ToClientModel(CreateClientModel clientModel)
        {
            var userModel = ToUserModel(clientModel.User, false);
            return new Client()
            {
                User = userModel,
                PhoneNumber = clientModel.PhoneNumber
            };
        }

        private static User ToUserModel(CreateUserModel createUserModel, bool isAdmin)
        {
            var userModel = new User()
            {
                Username = createUserModel.Username,
                Description = createUserModel.Description,
                FirstName = createUserModel.FirstName,
                LastName = createUserModel.LastName,
                Email = createUserModel.Email,
                IsAdmin = isAdmin,
                Password = createUserModel.Password
            };
            return userModel;
        }

        private string UpdateEmployeeDescription(Employee employee)
        {
            var documentDescription = string.Empty;
            var checkFileExists = File.Exists($"EmployeesDescription/{employee.Id}.txt");
            if (checkFileExists)
            {
                documentDescription = File.ReadAllText($"EmployeesDescription/{employee.Id}.txt");
            }
            return documentDescription;
        }
    }
}
