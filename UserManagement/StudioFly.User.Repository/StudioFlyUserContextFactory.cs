﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StudioFly.Users.EntityFramework;

namespace StudioFly.Users.Repository
{
    /// <summary>
    /// This class initialize the db connection
    /// </summary>
    public class StudioFlyContextFactory : IDesignTimeDbContextFactory<StudioFlyUserContext>
    {
        private static string _connectionString;

        /// <summary>
        /// initialize the db context
        /// </summary>
        /// <returns></returns>
        public StudioFlyUserContext CreateDbContext()
        {
            return CreateDbContext(null);
        }

        /// <summary>
        /// Creates the db context
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public StudioFlyUserContext CreateDbContext(string[] args)
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                LoadConnectionString();
            }

            var builder = new DbContextOptionsBuilder<StudioFlyUserContext>();
            builder.UseSqlServer(_connectionString);

            return new StudioFlyUserContext(builder.Options);
        }

        /// <summary>
        /// Load the connection string from appsettings.json file
        /// </summary>
        private static void LoadConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            _connectionString = configuration.GetConnectionString("StudioFlyUser");

        }
    }
}
