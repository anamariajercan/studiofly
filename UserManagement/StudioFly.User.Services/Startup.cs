﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudioFly.Users.EntityFramework;
using StudioFly.Users.Repository;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StudioFly.Users.Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            const string identityServiceUrl = "https://localhost:22001/";
            var xmlCommentsFile = $"{Assembly.GetEntryAssembly()?.GetName().Name}.xml";
            var xmlCommentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
            services.AddMvcCore(setupAction =>
                {
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
                    setupAction.Filters.Add(
                        new ProducesDefaultResponseTypeAttribute());
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status401Unauthorized));

                    setupAction.Filters.Add(
                        new AuthorizeFilter());

                    setupAction.ReturnHttpNotAcceptable = true;
                    setupAction.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                })
                .AddApiExplorer()
                .AddAuthorization()
                .AddJsonFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddSingleton<IUserManagement, UserManagement>();
            services.AddApiVersioning(SetupApiVersionAction());
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, ConfigureJwtBearerOptions(identityServiceUrl));
            services.AddSwaggerGen(SetupSwaggerAction(xmlCommentsFullPath));
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
         }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseExceptionHandler(appBuilder => appBuilder.Run(async context =>
                {
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
                }));
            }
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
            app.UseMvcWithDefaultRoute();
            app.UseSwagger();
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/StudioFlyUserManagement/swagger.json",
                    "Studio Fly User Management API");
                setupAction.OAuthClientId("swaggerui");
                setupAction.OAuthRealm("test-realm");
                setupAction.OAuthAppName("studioFly");
                setupAction.OAuthScopeSeparator(" ");
                setupAction.OAuthUseBasicAuthenticationWithAccessCodeGrant();
            });
        }

        private static Action<ApiVersioningOptions> SetupApiVersionAction()
        {
            return o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            };
        }

        private static Action<SwaggerGenOptions> SetupSwaggerAction(string xmlCommentsFullPath)
        {
            return options =>
            {
                options.SwaggerDoc("StudioFlyUserManagement",
                    new OpenApiInfo()
                    {
                        Title = "Studio Fly User Management API",
                        Version = "1"
                    });
                options.IncludeXmlComments(xmlCommentsFullPath);
                options.OperationFilter<AddAuthHeaderOperationFilter>();
                var securitySchema = new OpenApiSecurityScheme
                {
                    Scheme = "oauth2",
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = new Uri("https://localhost:22001/connect/authorize"),
                            TokenUrl = new Uri("https://localhost:22001/connect/token"),
                            Scopes = new Dictionary<string, string>
                            {
                                {"studioFlyApi", "studioFlyApi"}
                            }
                        }
                    }
                };
                options.AddSecurityDefinition("oauth2", securitySchema);

            };
        }

        private static Action<JwtBearerOptions> ConfigureJwtBearerOptions(string identityServiceUrl)
        {
            return o =>
            {
                o.RequireHttpsMetadata = false;
                o.Authority = identityServiceUrl;
                o.Audience = "studioFlyApi";
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateLifetime = false,
                    ValidateAudience = false
                };
            };
        }
    }
    internal class AddAuthHeaderOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var isAuthorized = (context.MethodInfo.DeclaringType.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any()
                                || context.MethodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any())
                               && !context.MethodInfo.GetCustomAttributes(true).OfType<AllowAnonymousAttribute>().Any(); // this excludes methods with AllowAnonymous attribute

            if (!isAuthorized) return;

            operation.Responses.TryAdd("401", new OpenApiResponse { Description = "Unauthorized" });
            operation.Responses.TryAdd("403", new OpenApiResponse { Description = "Forbidden" });

            var jwtbearerScheme = new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
            };

            operation.Security = new List<OpenApiSecurityRequirement>
            {
                new OpenApiSecurityRequirement { [jwtbearerScheme] = new string []{} }
            };
        }
    }
}