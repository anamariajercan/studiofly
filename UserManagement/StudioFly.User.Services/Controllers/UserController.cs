﻿using System;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudioFly.Users.EntityFramework;
using StudioFly.Users.EntityFramework.Model;

namespace StudioFly.Users.Services.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Authorize]
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserManagement _userManagement;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserController(IUserManagement userManagement, ILogger<UserController> logger)
        {
            _userManagement = userManagement;
            _logger = logger;
        }

        /// <summary>Gets the user based on a user id.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A <see cref="User"/> object containing user content.</returns>
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetUser(int id)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var helloContent = _userManagement.GetUser(id);
                return Ok(helloContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUser failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetHello failed");
            }
        }

        /// <summary>Gets the user based on a userName.</summary>
        /// <param name="userName">The identifier.</param>
        /// <returns>A <see cref="User"/> object containing user content.</returns>
        [AllowAnonymous]
        [HttpGet("{userName}")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetUserBasedOnUserName(string userName)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var user = _userManagement.GetUser(userName);
                return Ok(user);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUser failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetHello failed");
            }
        }

        /// <summary>Gets the user based on a userName and a password.</summary>
        /// <param name="userModel"></param>
        /// <returns>A <see cref="User"/> object containing user content.</returns>
        [AllowAnonymous]
        [HttpGet("basedonusernameandpassword")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetUserBasedOnUserNameAndPassword([FromBody] User userModel)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var user = _userManagement.GetUser(userModel);
                return Ok(user);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUser failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetHello failed");
            }
        }

        /// <summary>Gets the users from the database.</summary>
        /// <returns>A <see cref="User"/> object containing a list of users content.</returns>
        [AllowAnonymous]
        [HttpGet("users")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetUsers()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var helloContent = _userManagement.GetAllUsers();
                return Ok(helloContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetHello failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetHello failed");
            }
        }

        /// <summary>Gets the employees from the database.</summary>
        /// <returns>A <see cref="Employee"/> object containing a list of employees content.</returns>
        [AllowAnonymous]
        [HttpGet("employees")]
        [ProducesResponseType(typeof(Employee), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetEmployees()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var helloContent = _userManagement.GetAllEmployees();
                return Ok(helloContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetEmployees failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetEmployees failed");
            }
        }

        /// <summary>Gets the clients from the database.</summary>
        /// <returns>A <see cref="User"/> object containing a list of clients content.</returns>
        [AllowAnonymous]
        [HttpGet("clients")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetClients()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var helloContent = _userManagement.GetAllClients();
                return Ok(helloContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetClients failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetClients failed");
            }
        }

        /// <summary>
        /// <summary>Gets the EmployeePortfolios from the database.</summary>
        /// </summary>
        /// <returns>A <see cref="EmployeePortfolio"/> object containing EmployeePortfolio content.</returns>
        [AllowAnonymous]
        [HttpGet("employeesPortfolio")]
        [ProducesResponseType(typeof(EmployeePortfolio), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetEmployeesPortfolio()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var helloContent = _userManagement.GetEmployeesPortfolio();
                return Ok(helloContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetEmployeesPortfolio failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetEmployeesPortfolio failed");
            }
        }

        /// <summary>Create a new client.</summary>
        /// <param name="clientModel"><see cref="Client">client details</see> to be created.</param>
        /// <response code="201">The newly created <see cref="Client">client model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="403">User does not have write permission to the specified folder.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("create/client")]
        [ProducesResponseType(typeof(Client), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Create([FromBody] CreateClientModel clientModel)
        {
            var created=new Client();
            try
            {
                created = _userManagement.CreateClient(clientModel);
                return Created(new Uri($"/user/{created.Id}", UriKind.Relative), created);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"Get created client failed for {created.Id}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "Get created failed failed");
            }
        }

        /// <summary>
        /// Deletes a client from the database
        /// </summary>
        /// <param name="clientId">The method version Id.</param>
        /// <response code="200">Method draft version was successfully discarded.</response>
        /// <response code="400">The request is not valid</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpDelete("delete/{clientId}")]
        [ProducesResponseType(typeof(Client), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult DeleteClient(int clientId)
        {
            try
            {
                _userManagement.DeleteClient(clientId);
                return Ok();
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"Delete client failed for client id {clientId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "Delete client failed");
            }
        }

        private void ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid parameters");
            }
        }

        private IActionResult LogAndReturnUnauthorizedStatus(Exception ex, string message)
        {
            _logger.LogError(ex, message);
            return Unauthorized();
        }

        private IActionResult LogAndReturnBadRequestStatus(Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            ModelState.AddModelError("123", ex.Message);
            return BadRequest(ModelState);
        }

        private IActionResult LogAndReturnInternalServerErrorStatus(Exception exception, string errorMessage)
        {
            _logger.LogCritical(exception, $"{errorMessage}");
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}