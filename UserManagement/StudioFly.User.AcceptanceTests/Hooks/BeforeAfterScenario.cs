﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StudioFly.Users.AcceptanceTests.Utils;
using StudioFly.Users.Repository;
using TechTalk.SpecFlow;

namespace StudioFly.Users.AcceptanceTests.Hooks
{
    [Binding]
    public class BeforeAfterScenario
    {
        private static UserTokenContext _userTokenContext;
        public const string IdentityTokenUrl = "https://localhost:22001/connect/token";
        private static readonly UserManagement UserManagement = new UserManagement();
        private StudioFlyContext _studioFlyContext;
        public BeforeAfterScenario(UserTokenContext userTokenContext, StudioFlyContext studioFlyContext)
        {
            _userTokenContext = userTokenContext;
            _studioFlyContext = studioFlyContext;

        }

        [BeforeScenario]
        public async Task GetUserToken()
        {
            if (string.IsNullOrEmpty(_userTokenContext.AccessToken))
                _userTokenContext.AccessToken = await GetAuthTokenAsync("studioFly_roic", "Mimi", "test").ConfigureAwait(false);

        }

        [AfterScenario]
        public void CleanUp()
        {
            foreach (var client in _studioFlyContext.ItemsToDelete)
            {
                UserManagement.DeleteClient(int.Parse(client));
            }
        }

        public static async Task<string> GetAuthTokenAsync(string clientId, string username, string password, string identityTokenEndpoint = IdentityTokenUrl)
        {
            var req = new HttpRequestMessage(HttpMethod.Post, identityTokenEndpoint)
            {
                Content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    {"client_id", clientId},
                    {"client_secret", "secret"},
                    {"grant_type", "password"},
                    {"username", username},
                    {"password", password},
                    {"scope" ,"studioFlyApi"}
                })
            };

            HttpClient http = new HttpClient();
            var res = await http.SendAsync(req).ConfigureAwait(false);

             if (res.IsSuccessStatusCode)
            {
                var definition = new { access_token = "" };
                var jsonString = await res.Content.ReadAsStringAsync().ConfigureAwait(false);
                var token = JsonConvert.DeserializeAnonymousType(jsonString, definition);
                return token.access_token;
            }
            else
            {
                Console.WriteLine("Failed to get the studio fly access token! " + res.ReasonPhrase);
                return string.Empty;
            }
        }

    }
}