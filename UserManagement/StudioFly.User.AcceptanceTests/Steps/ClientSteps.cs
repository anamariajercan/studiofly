﻿//************************************************************************************************
// Copyright © 2019 Waters Corporation. All Rights Reserved.
//
//************************************************************************************************

using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudioFly.Users.AcceptanceTests.Utils;
using StudioFly.Users.EntityFramework.Model;
using TechTalk.SpecFlow;

namespace StudioFly.Users.AcceptanceTests.Steps
{
    [Binding]
    public sealed class ClientSteps
    {
        private readonly UserTokenContext _userTokenContext;
        private readonly StudioFlyContext _studioFlyContext;
        private readonly HttpHelper _httpHelper;

        public ClientSteps(UserTokenContext userTokenContext,
            StudioFlyContext studioFlyContext, HttpHelper httpHelper)
        {
            _userTokenContext = userTokenContext;
            _studioFlyContext = studioFlyContext;
            _httpHelper = httpHelper;
        }

        [Given(@"a client is already present in the data store")]
        [When(@"an client creation is requested")]
        public async Task CreateClients()
        {
            var appModel = InitiateCreateClientModel();
            var client = await _httpHelper.CreateClient(appModel, _userTokenContext.AccessToken).ConfigureAwait(true);
            if (client != null)
            {
                _studioFlyContext.Client = client;
                _studioFlyContext.ItemsToDelete.Add(client.Id.ToString());
            }
        }

        [When(@"a client deletion is requested")]
        public async Task WhenAClientDeletionIsRequested()
        {
            await _httpHelper.DeleteClient(_studioFlyContext.Client.Id, _userTokenContext.AccessToken).ConfigureAwait(true);
        }

        [Then(@"the client is deleted")]
        public async Task ThenTheClientIsDeleted()
        {
            var client = await _httpHelper.GetClient(_studioFlyContext.Client.Id.ToString(), _userTokenContext.AccessToken).ConfigureAwait(false);
            Assert.IsNull(client, "The client is not available in the data store");
        }

        private CreateClientModel InitiateCreateClientModel()
        {
            var clientModel = new CreateClientModel()
            {
                User = new CreateUserModel()
                {
                    Username = "Test",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    Description = "test description",
                    Email = "ana@test.com",
                    IsAdmin = false,
                    Password = "test"
                },
               PhoneNumber = "021234333",
            };

            return clientModel;
        }
    }
}
