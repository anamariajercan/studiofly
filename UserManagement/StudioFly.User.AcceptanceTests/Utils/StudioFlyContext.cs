﻿using System.Collections.Generic;

namespace StudioFly.Users.AcceptanceTests.Utils
{
	/// <summary>
	/// Class used to share the properties needed between steps
	/// </summary>
	public class StudioFlyContext
	{
        public StudioFlyContext()
        {
            ItemsToDelete = new List<string>();
        }

        public List<string> ItemsToDelete { get;}

        public Users.EntityFramework.Model.Client Client { get; set; }

	}
}
