﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StudioFly.Users.EntityFramework;

namespace StudioFly.Users.AcceptanceTests.Utils
{
    public class StudioFlyDbContextFactory : IDesignTimeDbContextFactory<StudioFlyUserContext>
    {
        private static string _connectionString;
        public StudioFlyUserContext CreateDbContext()
        {
            return CreateDbContext(null);
        }
        public StudioFlyUserContext CreateDbContext(string[] args)
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                LoadConnectionString();
            }

            var builder = new DbContextOptionsBuilder<StudioFlyUserContext>();
            builder.UseSqlServer(_connectionString);

            return new StudioFlyUserContext(builder.Options);
        }

        private static void LoadConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            _connectionString = configuration.GetConnectionString("StudioFly");
        }
    }
}
