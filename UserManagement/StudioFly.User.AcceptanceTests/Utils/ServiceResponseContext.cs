﻿using System.Net;
using System.Net.Http;

namespace StudioFly.Users.AcceptanceTests.Utils
{
    public sealed class ServiceResponseContext
    {
        public HttpResponseMessage Response { get; set; }

        public HttpContent Content => Response?.Content;

        public HttpStatusCode StatusCode => Response.StatusCode;

        public bool IsSuccessful => Response != null && Response.IsSuccessStatusCode;
    }
}