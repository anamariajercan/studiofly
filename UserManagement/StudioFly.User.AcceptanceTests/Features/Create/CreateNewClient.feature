﻿Feature: CreateNewClient
	As a client
	I want to persist an appointment the data store

Scenario: Create A New Client
	When an client creation is requested
	Then the service shall return status code 201
		And a client is created
