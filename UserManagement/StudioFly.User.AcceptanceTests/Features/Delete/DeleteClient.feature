﻿Feature: DeleteClient
	As a client
	I want to persist an appointment the data store

Scenario: DeleteClient
	Given a client is already present in the data store
	When a client deletion is requested
	Then the service shall return status code 200
		And the client is deleted
