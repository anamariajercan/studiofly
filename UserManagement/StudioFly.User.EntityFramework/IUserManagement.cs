﻿using StudioFly.Users.EntityFramework.Model;
using System.Collections.Generic;

namespace StudioFly.Users.EntityFramework
{
    /// <summary>
    /// Gets hello. Primary port.
    /// </summary>
    public interface IUserManagement
    {
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        User GetUser(int id);

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        User GetUser(User userModel);

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        User GetUser(string userName);

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Client GetClient(int id);
        /// <summary>
        /// Gets the employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Employee GetEmployee(int id);
        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Gets all Employees.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Employee> GetAllEmployees();

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="user">The user model.</param>
        /// <returns></returns>
        User AddUser(User user);
        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user model.</param>
        /// <returns></returns>
        User UpdateUser(User user);
        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool DeleteUser(int id);

        /// <summary>
        /// Gets All Clients
        /// </summary>
        /// <returns></returns>
        IEnumerable<Client> GetAllClients();

        /// <summary>
        /// Gets All Employees Portfolio
        /// </summary>
        /// <returns></returns>
        IEnumerable<EmployeePortfolio> GetEmployeesPortfolio();

        /// <summary>
        /// Creates the client.
        /// </summary>
        /// <param name="clientModel">The client model.</param>
        /// <returns></returns>
        Client CreateClient(CreateClientModel clientModel);

        /// <summary>
        /// Deletes the client.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        bool DeleteClient(int clientId);
    }
}
