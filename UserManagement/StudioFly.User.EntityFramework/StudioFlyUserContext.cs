﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using StudioFly.Users.EntityFramework.Model;

namespace StudioFly.Users.EntityFramework
{
    public class StudioFlyUserContext : DbContext
    {
        public StudioFlyUserContext() : base()
        {
            
        }
        public StudioFlyUserContext(DbContextOptions<StudioFlyUserContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeePortfolio> EmployeePortfolios { get; set; }
        public DbSet<Client> Clients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
