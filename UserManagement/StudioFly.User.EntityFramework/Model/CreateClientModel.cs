﻿namespace StudioFly.Users.EntityFramework.Model
{
    public class CreateClientModel
    {
        public string PhoneNumber { get; set; }
        public int UserId { get; set; }
        public CreateUserModel User { get; set; }
    }
}