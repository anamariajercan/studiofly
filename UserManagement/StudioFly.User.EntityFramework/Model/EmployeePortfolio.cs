﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Users.EntityFramework.Model
{
    public class EmployeePortfolio
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string PortfolioPath { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}
