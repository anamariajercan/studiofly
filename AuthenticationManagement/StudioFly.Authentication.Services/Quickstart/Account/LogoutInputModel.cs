﻿namespace StudioFly.Authentication.Services.Quickstart.Account
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
