namespace StudioFly.Authentication.Services.Quickstart.Account
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}