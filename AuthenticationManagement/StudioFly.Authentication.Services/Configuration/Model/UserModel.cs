﻿using System;
using System.Collections.Generic;

namespace StudioFly.Authentication.Services.Configuration.Model
{
    [Serializable]
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public int Id { get; set; }
        public ICollection<EmployeeModel> Employees { get; set; }
        public ICollection<ClientModel> Clients { get; set; }
    }
}