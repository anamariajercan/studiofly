﻿using System;
using System.Collections.Generic;

namespace StudioFly.Authentication.Services.Configuration.Model
{
    [Serializable]
    public sealed class EmployeeModel
    {
        //public EmployeeModel()
        //{
        //}

        //public EmployeeModel(string userName, string description, List<string> employeePortfolio)
        //{
        //    User = new UserModel {Username = userName, Description = description};
        //    EmployeePortfolio = new EmployeePortfolio();
        //    foreach (var portfolio in employeePortfolio)
        //    {
        //        EmployeePortfolio.PortfolioPath = portfolio;
        //    }
        //}
        public int Id { get; set; }

        public int UserId { get; set; }

        public UserModel User { get; set; }
        public List<EmployeePortfolio>EmployeePortfolio
        {
            get => _employeePortfolio;
            set
            {
                _employeePortfolio = value;
                _employeePortfolio = new List<EmployeePortfolio>();
            }
        }

        private List<EmployeePortfolio> _employeePortfolio;
    }
}
