﻿namespace StudioFly.Authentication.Services.Configuration.Model
{
    public class EmployeePortfolio
    {
        public int Id { get; set; }
        public string PortfolioPath { get; set; }
        public int EmployeeId { get; set; }
        public EmployeeModel Employee { get; set; }
    }
}
