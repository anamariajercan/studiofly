﻿namespace StudioFly.Authentication.Services.Configuration.Model
{
    public class ClientModel
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public int UserId { get; set; }
        public virtual UserModel User { get; set; }
    }
}
