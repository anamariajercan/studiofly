﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace StudioFly.Authentication.Services.Configuration
{
    public class InMemoryConfiguration
    {
        public static IEnumerable<ApiResource> ApiResources()
        {
            return new[] {
                new ApiResource("studioFlyApi", "StudioFly")
            };
        }


        public static IEnumerable<IdentityResource> IdentityResources()
        {
            return new IdentityResource[] {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        public static IEnumerable<Client> Clients()
        {
            return new[]
            {
                 new Client
                {
                    ClientId = "studioFly_code",
                    ClientSecrets = new [] { new Secret("secret".Sha256()) },
                    ClientUri="https://localhost:23001",
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    AllowedScopes = new [] {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "studioFlyApi"
                    },
                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                    RedirectUris = new []
                    {
                        "https://localhost:23001/signin-oidc",
                        "https://localhost:25001/swagger/oauth2-redirect.html"
                    },
                    PostLogoutRedirectUris = { "https://localhost:23001/signout-callback-oidc" },
                },
                new Client
                {
                    ClientId="swaggerui",
                    ClientName = "Swagger UI",
                    AllowedGrantTypes=GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser=true,
                    RedirectUris = { "https://localhost:25001/swagger/oauth2-redirect.html","https://localhost:24001/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris={ "https://localhost:23001/signout-callback-oidc" },
                    AllowedScopes = { "studioFlyApi" }
                },
                new Client
                {
                    ClientId = "studioFly_roic",
                    ClientSecrets = new [] { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = new [] {
                        "studioFlyApi"
                    }
                }
            };
        }
    }
}
