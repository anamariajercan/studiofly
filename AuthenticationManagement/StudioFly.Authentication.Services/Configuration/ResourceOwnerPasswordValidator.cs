﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System.Threading.Tasks;
using StudioFly.Authentication.Services.Configuration.Model;
using StudioFly.Authentication.Services.Configuration.Provider;

namespace StudioFly.Authentication.Services.Configuration
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IUserProvider _repository;

        public ResourceOwnerPasswordValidator(IUserProvider repository)
        {
            _repository = repository;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            UserModel userModel= new UserModel()
            {
                Username = context.UserName,
                Password= HashHelper.Sha512(context.Password + context.UserName)
            };
            var user = await _repository.ValidateCredentialsAsync(userModel);

            if (user != null)
            {
                context.Result = new GrantValidationResult(user.Id.ToString(),
                    authenticationMethod: "custom");
            }
            else
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, 
                    "Invalid Credentials");
            }
        }
    }
}
