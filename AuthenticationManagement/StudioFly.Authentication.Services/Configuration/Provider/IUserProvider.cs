﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using StudioFly.Authentication.Services.Configuration.Model;

namespace StudioFly.Authentication.Services.Configuration.Provider
{
    public interface IUserProvider
    {
        Task<UserModel> ValidateCredentialsAsync(UserModel userModel);
        Task<UserModel> FindByUsernameAsync(string username);
        Task<UserModel> FindByExternalProviderAsync(string provider, string userId);
        Task<UserModel> AutoProvisionUserAsync(string provider, string userId, IEnumerable<Claim> claims);

    }
}
