﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StudioFly.Authentication.Services.Configuration.Model;

namespace StudioFly.Authentication.Services.Configuration.Provider
{
    public class UserProxy : Proxy, IUserProxy
    {
        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="httpClient"></param>
        /// <param name="settings"></param>
        /// <param name="accessor"></param>
        public UserProxy(ILogger<Proxy> logger, HttpClient httpClient, UserSettings settings, IHttpContextAccessor accessor)
            : base(logger, httpClient, accessor, new Uri(settings.ServiceBaseAddress, "v1.0/"))
        {
        }

        public async Task<UserModel> GetUserBasedOnUserNameAndPassword(UserModel userModel)
        {
            var uri = new Uri("user/basedonusernameandpassword", UriKind.Relative);
            return await Send<UserModel>(HttpMethod.Get, uri,userModel).ConfigureAwait(false);
        }
        public async Task<UserModel> GetUserBasedOnUserName(string userName)
        {
            var uri = new Uri($"user/{userName}", UriKind.Relative);
            return await Send<UserModel>(HttpMethod.Get, uri).ConfigureAwait(false);
        }
    }
}
