﻿using System.Threading.Tasks;
using StudioFly.Authentication.Services.Configuration.Model;

namespace StudioFly.Authentication.Services.Configuration.Provider
{
    public interface IUserProxy
    {
        Task<UserModel> GetUserBasedOnUserNameAndPassword(UserModel userModel);
        Task<UserModel> GetUserBasedOnUserName(string userName);

    }
}
