﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using StudioFly.Authentication.Services.Configuration.Model;

namespace StudioFly.Authentication.Services.Configuration.Provider
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserProxy _proxy;

        public UserProvider(IUserProxy proxy)
        {
            _proxy = proxy;
        }

        public Task<UserModel> AutoProvisionUserAsync(string provider, string userId, IEnumerable<Claim> claims)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> FindByExternalProviderAsync(string provider, string userId)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> FindByUsernameAsync(string username)
        {
            return _proxy.GetUserBasedOnUserName(username);
        }

        public async Task<UserModel> ValidateCredentialsAsync(UserModel userModel)
        {
            return await _proxy.GetUserBasedOnUserNameAndPassword(userModel).ConfigureAwait(false);
        }
    }
}
