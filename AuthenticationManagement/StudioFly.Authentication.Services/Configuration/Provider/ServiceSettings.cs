﻿using System;
using System.Globalization;

namespace StudioFly.Authentication.Services.Configuration.Provider
{
    public class ServiceSettings
    {
        public Uri ServiceBaseAddress { get; }

        public ServiceSettings(string serviceBaseAddress, string host = "localhost")
        {
            ServiceBaseAddress = new Uri(string.Format(CultureInfo.InvariantCulture, serviceBaseAddress, host), UriKind.RelativeOrAbsolute);
        }

    }
    public sealed class UserSettings : ServiceSettings
    {
        public UserSettings(string baseAddress) : base(baseAddress) { }
    }
    public sealed class AppointmentSettings : ServiceSettings
    {
        public AppointmentSettings(string baseAddress) : base(baseAddress) { }
    }
}
