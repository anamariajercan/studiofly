﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StudioFly.Authentication.Services.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.DbContexts;
using StudioFly.Authentication.Services.Configuration.Provider;

namespace StudioFly.Authentication.Services
{
    public class Startup
    {
        private const string ConnectionString = "StudioFlyAuthentication";
        private readonly ILogger _logger;

        /// <summary>Initializes a new instance of the <see cref="Startup"/> class.</summary>
        /// <param name="env">The env.</param>
        /// <param name="logger">The logger instance.</param>
        public Startup(IHostingEnvironment env, ILogger<Startup> logger)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           var connectionString = Configuration.GetConnectionString(ConnectionString);
            services.AddHttpClient<IUserProxy, UserProxy>();
            services.AddTransient<IUserProvider, UserProvider>();
            services.AddSingleton(new UserSettings(Configuration["UserManagementUrl"]));
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            _logger.LogInformation("Configuring services");
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            Console.WriteLine($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\CertificateScript\\StudioFlyClient.pfx");
            services.AddIdentityServer()
                .AddSigningCredential(new X509Certificate2(
                    $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\CertificateScript\\StudioFlyClient.pfx",
                    "studioFly"))
                .AddDeveloperSigningCredential()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddConfigurationStore(options => options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                    sql => sql.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(options => options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                    sql => sql.MigrationsAssembly(migrationsAssembly)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            MigrateInMemoryDataToSqlServer(app);
            app.UseDeveloperExceptionPage();
            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
            app.UseHttpsRedirection();
        }

        public void MigrateInMemoryDataToSqlServer(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
                var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                context.Database.Migrate();
                if (!context.Clients.Any())
                {
                    foreach (var client in InMemoryConfiguration.Clients())
                    {
                        context.Clients.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }
                if (!context.IdentityResources.Any())
                {
                    foreach (var resource in InMemoryConfiguration.IdentityResources())
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
                if (context.ApiResources.Any()) return;
                {
                    foreach (var resource in InMemoryConfiguration.ApiResources())
                    {
                        context.ApiResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}
