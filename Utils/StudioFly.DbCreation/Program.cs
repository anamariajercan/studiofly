﻿using System;

namespace StudioFly.DbCreation
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (var sha = System.Security.Cryptography.SHA512.Create())
            //{
            //    var bytes = System.Text.Encoding.UTF8.GetBytes("testDiana");
            //    var hash = sha.ComputeHash(bytes);

            //    var test = Convert.ToBase64String(hash);
            //}
            using (var db = new StudioFlyAppointmentContextFactory().CreateDbContext())
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
                Console.WriteLine("Press any key to exit...");
            }
            //using (var db = new StudioFlyUserContextFactory().CreateDbContext())
            //{
            //    db.Database.EnsureDeleted();
            //    db.Database.EnsureCreated();
            //    Console.WriteLine("Press any key to exit...");
            //}
        }
    }
}
