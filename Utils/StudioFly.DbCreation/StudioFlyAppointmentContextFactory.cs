﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StudioFly.Appointments.EntityFramework;

namespace StudioFly.DbCreation
{
    public class StudioFlyAppointmentContextFactory : IDesignTimeDbContextFactory<StudioFlyAppointmentContext>
    {
        private static string _connectionString;
        public StudioFlyAppointmentContext CreateDbContext()
        {
            return CreateDbContext(null);
        }
        public StudioFlyAppointmentContext CreateDbContext(string[] args)
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                LoadConnectionString();
            }

            var builder = new DbContextOptionsBuilder<StudioFlyAppointmentContext>();
            builder.UseSqlServer(_connectionString);

            return new StudioFlyAppointmentContext(builder.Options);
        }

        private static void LoadConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            _connectionString = configuration.GetConnectionString("StudioFlyAppointment");
        }
    }
}
