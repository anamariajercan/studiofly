﻿var calendar, selectedCalendar, datePicker, resizeThrottled, employees, clients, loggedInUser;
var createEvent, datepicker, clickEvent, datepicker2;
var features = [];
var CalendarList = [];
$(async function () {
    var appointmentsData = [];
    getClients();
    getEmployees();
    $.ajax({
        url: "/api/v1.0/appointment/appointments"
    }).then(function (appointments) {
        generateCalendar();
        var startDate = new Date();
        var setupDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));
        var setupFinishDate = new Date(startDate.getTime() + (7 * 24 * 60 * 60 * 1000));
        var userId = $('#userId').val();
        loggedInUser = getUserBasedOnId(userId);
        $.each(appointments,
            function (key, appointmentValue) {
                const appointmentTimeStart = new Date(appointmentValue.appointmentTimeStart);
                if (appointmentTimeStart > setupDate &&
                    appointmentTimeStart < setupFinishDate)
                    generateAppointments(key, appointmentValue);
            });
        calendar = initializeCalendar();

        calendar.createSchedules(appointmentsData);
        setDropdownCalendarType();
        setRenderRangeText();
        setEventListener();
        calendar.on({
            'clickSchedule': function (e) {
                console.log("clickSchedule", e);
                clickEvent = e;
            },
            'beforeCreateSchedule': function (e) {
                console.log("beforeCreateSchedule", e);
                $('#datepicker-input')
                    .val(e.start._date.toLocaleDateString() + ' ' + e.start._date.toLocaleTimeString());
                $('#datepicker-input-2')
                    .val(e.end._date.toLocaleDateString() + ' ' + e.end._date.toLocaleTimeString());
                $('#createSchedule').modal();
                createEvent = e;
            },
            'beforeUpdateSchedule': function (e) {
                console.log('beforeUpdateSchedule', e);
                $('#datepicker-input').val(clickEvent.schedule.start._date.toLocaleDateString() +
                    ' ' +
                    clickEvent.schedule.start._date.toLocaleTimeString());
                $('#datepicker-input-2').val(clickEvent.schedule.end._date.toLocaleDateString() +
                    ' ' +
                    clickEvent.schedule.end._date.toLocaleTimeString());
                $('#createSchedule').modal();
                createEvent = e;
            },
            'beforeDeleteSchedule': function (e) {
                console.log('beforeDeleteSchedule', e);
                calendar.deleteSchedule(e.schedule.id, e.schedule.calendarId);
                deleteAppointment(e.schedule.raw);
                location.reload(true);
            },
            'clickMore': function (e) {
                console.log('clickMore', e);
            },
            'clickDayname': function (date) {
                console.log('clickDayname', date);
            },
            'afterRenderSchedule': function (e) {
                var schedule = e.schedule;
            },
            'clickTimezonesCollapseBtn': function (timezonesCollapsed) {
                console.log('timezonesCollapsed', timezonesCollapsed);

                if (timezonesCollapsed) {
                    calendar.setTheme({
                        'week.daygridLeft.width': '77px',
                        'week.timegridLeft.width': '77px'
                    });
                } else {
                    calendar.setTheme({
                        'week.daygridLeft.width': '60px',
                        'week.timegridLeft.width': '60px'
                    });
                }

                return true;
            },
        });
        $('#btn-save-schedule').on('click',
            function (e) {
                if (typeof clickEvent === 'undefined')
                    saveNewSchedule(createEvent);
                else {
                    updateExistingSchedule(clickEvent);
                }
                $('#createSchedule').modal('hide');
            });
        $('#appointmentType').on("change",
            function () {
                $('#feature').empty();
                getFeaturesBasedOnSelectedAppointmentTypeName();
            });
        $('#lnb-calendars').on('change', function (e) {
            onChangeCalendars(e);
        });
    })
        .then(function () {
            $.ajax({
                url: "/api/v1.0/appointment/appointmentTypes"
            })
                .then(function (data) {
                    $('#appointmentType').empty();
                    $.each(data,
                        function (key, appointmentTypeValue) {
                            $('#appointmentType').append($('<option></option>').val(appointmentTypeValue.name)
                                .html(appointmentTypeValue.name));
                            $('<input>',
                                {
                                    type: 'hidden',
                                    id: appointmentTypeValue.name,
                                    value: appointmentTypeValue.id
                                }).appendTo('form');
                        });
                })
                .then(function () {
                    getFeaturesBasedOnSelectedAppointmentTypeName();
                })
                .then(function () {
                    $('#employee').empty();
                    $.each(employees,
                        function (key, dataValue) {
                            $('#employee').append($('<option></option>').val(dataValue.user.username)
                                .html(dataValue.user.username));
                            $('<input>',
                                {
                                    type: 'hidden',
                                    id: dataValue.user.username,
                                    value: dataValue.id
                                }).appendTo('form');
                        });
                })
                .then(function () {
                    $('#client').empty();
                    $.each(clients,
                        function (key, data) {
                            $('#client').append($('<option></option>').val(data.user.username)
                                .html(data.user.username));
                        });
                })
                .then(function () {
                    datepicker = new tui.DatePicker('#wrapper',
                        {
                            date: new Date(),
                            input: {
                                element: '#datepicker-input',
                                format: 'M/dd/yyyy HH:mm A'
                            },
                            timepicker: true
                        });

                    datepicker2 = new tui.DatePicker('#wrapper-2',
                        {
                            date: new Date(),
                            input: {
                                element: '#datepicker-input-2',
                                format: 'M/dd/yyyy HH:mm A'
                            },
                            timepicker: {
                                layoutType: 'tab',
                                inputType: 'spinbox'
                            }
                        });

                });
        });
    resizeThrottled = tui.util.throttle(function () {
        calendar.render();
    },
        50);

    window.calendar = calendar;

    function deleteAppointment(id) {
        $.ajax({
            type: "DELETE",
            url: "/api/v1.0/appointment/deleteAppointment/" + id
        }).then(function (data) {
            location.reload(true);
        });
    }
    function generateAppointments(key, appointmentValue) {
        const schedule = {};
        schedule.id = key;
        schedule.raw = appointmentValue.appointmentModel.id;
        schedule.start = appointmentValue.appointmentTimeStart;
        schedule.end = appointmentValue.appointmentTimeEnd;
        const color = search(appointmentValue.employeeModel.id);
        schedule.calendarId = appointmentValue.employeeModel.id;
        schedule.category = 'time';
        schedule.bgColor = color;
        schedule.dragBgColor = color;
        schedule.borderColor = color;
        schedule.isReadOnly = true;
        if (loggedInUser.id === appointmentValue.clientModel.user.id || loggedInUser.isAdmin) {
            schedule.title = 'Clientul ' +
                appointmentValue.clientModel.user.username +
                ' la angajatul' +
                appointmentValue.employeeModel.user.username +
                ' pentru' +
                appointmentValue.appointmentTypeModel.name +
                ' ' +
                appointmentValue.featureModel.name;
            schedule.isReadOnly = false;
        }
        appointmentsData.push(schedule);
    }
    function initializeCalendar() {
        return new tui.Calendar('#calendar',
            {
                taskView: [],
                scheduleView: ['time'],
                defaultView: 'week',
                calendars:CalendarList,
                week: {
                    daynames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    startDayOfWeek: 1,
                    narrowWeekend: true,
                    hourStart: 8,
                    hourEnd: 18
                },
                // whether use default creation popup or not
                useCreationPopup: false,

                // whether use default detail popup or not
                useDetailPopup: true,
                milestone: function (model) {
                    return '<span class="calendar-font-icon ic-milestone-b"></span> <span style="background-color: ' +
                        model.bgColor +
                        '">' +
                        model.title +
                        '</span>';
                },
                allday: function (schedule) {
                    return getTimeTemplate(schedule, true);
                },
                time: function (schedule) {
                    return getTimeTemplate(schedule, false);
                }
            });
    }
    function saveNewSchedule(event) {
        var start = event.start ? new Date(event.start.getTime()) : new Date();
        var end = new Date($('#datepicker-input-2').val());
        var title = $('#appointmentType').val();
        var feature = $('#feature').val();
        var employeeName = $('#employee').val();
        var employee;
        $.each(employees,
            function (key, employeeValue) {
                if (employeeName == employeeValue.user.username) {
                    employee = employeeValue.id;
                }
            });
        var clientId;
        if (loggedInUser.isAdmin) {
            var client = $('#client').val();
            $.each(clients,
                function (key, clientValue) {
                    if (client == clientValue.user.username) {
                        clientId = clientValue.id;
                    }
                });
        } else {
            $.each(clients,
                function (key, clientValue) {
                    if (loggedInUser.username == clientValue.user.username) {
                        clientId = clientValue.id;
                    }
                });
        }
        var color = search(employee);
        var schedule = {
            id: genRand(),
            location: clientId,
            calendarId: employee,
            title: title,
            start: start.toISOString(),
            body: feature,
            end: end.toISOString(),
            bgColor: color,
            dragBgColor: color,
            borderColor: color
        }
        calendar.createSchedules([schedule]);
        refreshScheduleVisibility();
        createAppointment(schedule);
    }
    function updateExistingSchedule(event) {
        var start = new Date($('#datepicker-input').val());
        var end = new Date($('#datepicker-input-2').val());
        var title = $('#appointmentType').val();
        var feature = $('#feature').val();
        var employeeName = $('#employee').val();
        var employee;
        $.each(employees,
            function (key, employeeValue) {
                if (employeeName == employeeValue.user.username) {
                    employee = employeeValue.id;
                }
            });
        var clientId;
        if (loggedInUser.isAdmin) {
            var client = $('#client').val();
            $.each(clients,
                function (key, clientValue) {
                    if (client == clientValue.user.username) {
                        clientId = clientValue.id;
                    }
                });
        } else {
            $.each(clients,
                function (key, clientValue) {
                    if (loggedInUser.username == clientValue.user.username) {
                        clientId = clientValue.id;
                    }
                });
        }
        var color = search(employee);
        var schedule = {
            id: clickEvent.schedule.id,
            location: clientId,
            calendarId: employee,
            title: title,
            start: start.toISOString(),
            body: feature,
            end: end.toISOString(),
            raw: clickEvent.schedule.raw,
            bgColor: color,
            dragBgColor: color,
            borderColor: color
        }
        calendar.deleteSchedule(clickEvent.schedule.id, clickEvent.schedule.calendarId);
        calendar.createSchedules([schedule]);
        refreshScheduleVisibility();
        editAppointment(schedule);
    }
    function refreshScheduleVisibility() {
        var calendarElements = Array.prototype.slice.call(document.querySelectorAll('#calendarList input'));

        CalendarList.forEach(function (selectedCalendar) {
            calendar.toggleSchedules(selectedCalendar.id, !selectedCalendar.checked, false);
        });

        calendar.render(true);

        calendarElements.forEach(function (input) {
            var span = input.nextElementSibling;
            span.style.backgroundColor = input.checked ? span.style.borderColor : 'transparent';
        });
    }
    function genRand() {
        return Math.floor(Math.random() * 89999 + 10000);
    }
    function getFeaturesBasedOnSelectedAppointmentTypeName() {

        var appointmentTypeName = $('#appointmentType').val();
        $.ajax({
            url: "/api/v1.0/appointment/features/" + appointmentTypeName
        }).then(function (features) {
            $('#feature').empty();
            $.each(features,
                function (key, featureValue) {
                    $('#feature').append($('<option></option>').val(featureValue.name).html(featureValue.name));
                    $('<input>',
                        {
                            type: 'hidden',
                            id: featureValue.name,
                            value: featureValue.id
                        }).appendTo('form');
                });
        });
    }
    function createAppointment(schedule) {
        var userId = schedule.location;
        var appointmentType = $('#appointmentType').val();
        var appointmentTypeId = $('#' + appointmentType).val();
        var feature = $('#feature').val();
        var featureId = $('#' + feature).val();
        var featureAppointmentType = getFeatureAppointmentTypes(featureId, appointmentTypeId);
        var employee = $('#employee').val();
        var employeeId = $('#' + employee).val();
        var scheduleStart = schedule.start;
        $.ajax({
            type: "POST",
            url: "https://localhost:23001/api/v1.0/appointment/createAppointment",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (err) {
                console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
            },
            data: JSON.stringify({
                AppointmentTime: scheduleStart,
                EmployeeId: employeeId,
                ClientId: userId,
                FeatureAppointmentTypeId: featureAppointmentType.id
            })
        }).done(function (msg) {
            location.reload(true);
        });
    }
    function editAppointment(schedule) {
        var userId = schedule.location;
        var appointmentType = $('#appointmentType').val();
        var appointmentTypeId = $('#' + appointmentType).val();
        var feature = $('#feature').val();
        var featureId = $('#' + feature).val();
        var featureAppointmentType = getFeatureAppointmentTypes(featureId, appointmentTypeId);
        var employee = $('#employee').val();
        var employeeId = $('#' + employee).val();
        var scheduleStart = schedule.start;
        $.ajax({
            type: "POST",
            url: "https://localhost:23001/api/v1.0/appointment/editAppointment",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                Id: schedule.raw,
                OriginalId: schedule.raw,
                AppointmentTime: scheduleStart,
                EmployeeId: employeeId,
                ClientId: userId,
                FeatureAppointmentTypeId: featureAppointmentType.id
            })
        }).done(function (msg) {
            location.reload(true);
        });
    }
    function getFeatureAppointmentTypes(featureId, appointmentTypeId) {
        var featureAppointmentType;
        $.ajax({
            url: "/api/v1.0/appointment/featureAppointmentTypes",
            async: false,
            success: function (data) {
                $.each(data,
                    function (key, value) {
                        if (value.featureId == featureId && value.appointmentTypeId==appointmentTypeId ) {
                            featureAppointmentType = value;
                        }
                    });
            }
        });
        return featureAppointmentType;
    }
    function findCalendar(id) {
        var found;

        CalendarList.forEach(function (calendar) {
            if (calendar.id === id) {
                found = calendar;
            }
        });

        return found || CalendarList[0];
    }
    function getTimeTemplate(schedule, isAllDay) {
        var html = [];
        var start = moment(schedule.start.toUTCString());
        if (!isAllDay) {
            html.push('<strong>' + start.format('HH:mm') + '</strong> ');
        }
        if (schedule.isPrivate) {
            html.push('<span class="calendar-font-icon ic-lock-b"></span>');
            html.push(' Private');
        } else {
            if (schedule.isReadOnly) {
                html.push('<span class="calendar-font-icon ic-readonly-b"></span>');
            } else if (schedule.recurrenceRule) {
                html.push('<span class="calendar-font-icon ic-repeat-b"></span>');
            } else if (schedule.attendees.length) {
                html.push('<span class="calendar-font-icon ic-user-b"></span>');
            } else if (schedule.location) {
                html.push('<span class="calendar-font-icon ic-location-b"></span>');
            }
            html.push(' ' + schedule.title);
        }

        return html.join('');
    }
    function onClickMenu(e) {
        var target = $(e.target).closest('a[role="menuitem"]')[0];
        var action = getDataAction(target);
        var options = calendar.getOptions();
        var viewName = '';

        console.log(target);
        console.log(action);
        switch (action) {
            case 'toggle-daily':
                viewName = 'day';
                break;
            case 'toggle-weekly':
                viewName = 'week';
                break;
            case 'toggle-monthly':
                options.month.visibleWeeksCount = 0;
                viewName = 'month';
                break;
            case 'toggle-weeks2':
                options.month.visibleWeeksCount = 2;
                viewName = 'month';
                break;
            case 'toggle-weeks3':
                options.month.visibleWeeksCount = 3;
                viewName = 'month';
                break;
            case 'toggle-narrow-weekend':
                options.month.narrowWeekend = !options.month.narrowWeekend;
                options.week.narrowWeekend = !options.week.narrowWeekend;
                viewName = calendar.getViewName();

                target.querySelector('input').checked = options.month.narrowWeekend;
                break;
            case 'toggle-start-day-1':
                options.month.startDayOfWeek = options.month.startDayOfWeek ? 0 : 1;
                options.week.startDayOfWeek = options.week.startDayOfWeek ? 0 : 1;
                viewName = calendar.getViewName();

                target.querySelector('input').checked = options.month.startDayOfWeek;
                break;
            case 'toggle-workweek':
                options.month.workweek = !options.month.workweek;
                options.week.workweek = !options.week.workweek;
                viewName = calendar.getViewName();

                target.querySelector('input').checked = !options.month.workweek;
                break;
            default:
                break;
        }

        calendar.setOptions(options, true);
        calendar.changeView(viewName, true);

        setDropdownCalendarType();
        setRenderRangeText();
    }
    function onClickNavi(e) {
        var action = getDataAction(e.target);

        switch (action) {
            case 'move-prev':
                calendar.prev();
                break;
            case 'move-next':
                calendar.next();
                break;
            case 'move-today':
                calendar.today();
                break;
            default:
                return;
        }

        setRenderRangeText();
    }
    function onChangeNewScheduleCalendar(e) {
        var target = $(e.target).closest('a[role="menuitem"]')[0];
        var calendarId = getDataAction(target);
        changeNewScheduleCalendar(calendarId);
    }
    function changeNewScheduleCalendar(calendarId) {
        var calendarNameElement = document.getElementById('calendarName');
        var calendar = findCalendar(calendarId);
        var html = [];

        html.push('<span class="calendar-bar" style="background-color: ' +
            calendar.bgColor +
            '; border-color:' +
            calendar.borderColor +
            ';"></span>');
        html.push('<span class="calendar-name">' + calendar.name + '</span>');

        calendarNameElement.innerHTML = html.join('');

        selectedCalendar = calendar;
    }
    function createNewSchedule(event) {
        var start = event.start ? new Date(event.start.getTime()) : new Date();
        var end = event.end ? new Date(event.end.getTime()) : moment().add(1, 'hours').toDate();
        $('#createSchedule').modal();
    }
    function onChangeCalendars(e) {
        var calendarId = e.target.value;
        var checked = e.target.checked;
        var viewAll = document.querySelector('.lnb-calendars-item input');
        var calendarElements = Array.prototype.slice.call(document.querySelectorAll('#calendarList input'));
        var allCheckedCalendars = true;

        if (calendarId === 'all') {
            allCheckedCalendars = checked;

            calendarElements.forEach(function (input) {
                var span = input.parentNode;
                input.checked = checked;
                span.style.backgroundColor = checked ? span.style.borderColor : 'transparent';
            });

            CalendarList.forEach(function (calendar) {
                calendar.checked = checked;
            });
        } else {
            findCalendar(calendarId).checked = checked;

            allCheckedCalendars = calendarElements.every(function (input) {
                return input.checked;
            });

            if (allCheckedCalendars) {
                viewAll.checked = true;
            } else {
                viewAll.checked = false;
            }
        }

        refreshScheduleVisibility();
    }
    function setDropdownCalendarType() {
        var calendarTypeName = document.getElementById('calendarTypeName');
        var calendarTypeIcon = document.getElementById('calendarTypeIcon');
        var options = calendar.getOptions();
        var type = calendar.getViewName();
        var iconClassName;

        if (type === 'day') {
            type = 'Daily';
            iconClassName = 'calendar-icon ic_view_day';
        } else if (type === 'week') {
            type = 'Weekly';
            iconClassName = 'calendar-icon ic_view_week';
        } else if (options.month.visibleWeeksCount === 2) {
            type = '2 weeks';
            iconClassName = 'calendar-icon ic_view_week';
        } else if (options.month.visibleWeeksCount === 3) {
            type = '3 weeks';
            iconClassName = 'calendar-icon ic_view_week';
        } else {
            type = 'Monthly';
            iconClassName = 'calendar-icon ic_view_month';
        }

        calendarTypeName.innerHTML = type;
        calendarTypeIcon.className = iconClassName;
    }
    function setRenderRangeText() {
        var renderRange = document.getElementById('renderRange');
        var options = calendar.getOptions();
        var viewName = calendar.getViewName();
        var html = [];
        if (viewName === 'day') {
            html.push(moment(calendar.getDate().getTime()).format('YYYY.MM.DD'));
        } else if (viewName === 'month' &&
            (!options.month.visibleWeeksCount || options.month.visibleWeeksCount > 4)) {
            html.push(moment(calendar.getDate().getTime()).format('YYYY.MM'));
        } else {
            html.push(moment(calendar.getDateRangeStart().getTime()).format('YYYY.MM.DD'));
            html.push(' ~ ');
            html.push(moment(calendar.getDateRangeEnd().getTime()).format(' MM.DD'));
        }
        renderRange.innerHTML = html.join('');
    }
    function setEventListener() {
        $('#menu-navi').on('click', onClickNavi);
        $('.dropdown-menu a[role="menuitem"]').on('click', onClickMenu);
        $('#lnb-calendars').on('change', onChangeCalendars);

        $('#btn-new-schedule').on('click', createNewSchedule);

        $('#dropdownMenu-calendars-list').on('click', onChangeNewScheduleCalendar);

        window.addEventListener('resize', resizeThrottled);
    }
    function getDataAction(target) {
        return target.dataset ? target.dataset.action : target.getAttribute('data-action');
    }
    function getEmployees() {
        $.ajax({
            url: "/api/v1.0/users/employees",
            async: false,
            success: function (data) {
                employees = data;
            }
        });
    }
    function getClients() {
        $.ajax({
            url: "/api/v1.0/users/clients",
            async: false,
            success: function (clientsData) {
                clients = clientsData;
            }
        });
    }
    function getUserBasedOnId(id) {
        var user;
        $.ajax({
            async: false,
            url: "/api/v1.0/users/" + id,
            success: function (data) {
                user = data;
            }
        });
        return user;
    }
    function search(id) {
        for (var i = 0; i < CalendarList.length; i++) {
            var expectedId = CalendarList[i].id;
            if (expectedId === id) {
                return CalendarList[i].bgColor;
            }
        }
    }
    function generateCalendar() {
        var calendar, color;
        $.each(employees,
            function (key, dataValue) {
                calendar = new CalendarInfo();
                if (dataValue.user.username === "Mimi") {
                    color = "#8EF430";
                }
                else if (dataValue.user.username === "Katy") {
                    color = "#f4ec30";
                }
                else if (dataValue.user.username === "Lau") {
                    color = "#30f4e5";
                }
                calendar.id = dataValue.id;
                calendar.name = dataValue.user.username;
                calendar.color = '#ffffff';
                calendar.bgColor = color;
                calendar.dragBgColor = color;
                calendar.borderColor = color;
                addCalendar(calendar);
            });
        var calendarList = document.getElementById('calendarList');
        var html = [];
        $.each(CalendarList,
            function (cal, value) {
                html.push('<div class="lnb-calendars-item"><label>' +
                    '<input type="checkbox" class="tui-full-calendar-checkbox-round" value="' +
                    value.id +
                    '" checked>' +
                    '<span style="border-color: ' +
                    value.borderColor +
                    '; background-color: ' +
                    value.borderColor +
                    ';"></span>' +
                    '<span>' +
                    value.name +
                    '</span>' +
                    '</label></div>'
                );
            });
        calendarList.innerHTML = html.join('\n');
    }
    function CalendarInfo() {
        this.id = null;
        this.name = null;
        this.checked = true;
        this.color = null;
        this.bgColor = null;
        this.borderColor = null;
        this.dragBgColor = null;
    }
    function addCalendar(calendar) {
        CalendarList.push(calendar);
    }
});