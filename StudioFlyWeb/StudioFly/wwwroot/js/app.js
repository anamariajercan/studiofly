﻿var employees, clients;
var CalendarList = [];

$(function () {
    $.when(getClients(), getEmployees()).done(function() {
        generateCalendar();
    });
    function getEmployees() {
        $.ajax({
            url: "/api/v1.0/users/employees",
        })
            .then(function (data) {
                employees = data;
            });
    }
    function getClients() {
        $.ajax({
            url: "/api/v1.0/users/clients",
        })
            .then(function (clientsData) {
                clients = clientsData;
            });
    }

    function generateCalendar() {
        var calendar;
        $.each(employees,
            function (key, dataValue) {
                calendar = new CalendarInfo();
                var color = getRandomColor();
                calendar.id = dataValue.id;
                calendar.name = dataValue.user.username;
                calendar.color = '#ffffff';
                calendar.bgColor = color;
                calendar.dragBgColor = color;
                calendar.borderColor = color;
                addCalendar(calendar);
            });
        var calendarList = document.getElementById('calendarList');
        var html = [];
        $.each(CalendarList,
            function (cal, value) {
                html.push('<div class="lnb-calendars-item"><label>' +
                    '<input type="checkbox" class="tui-full-calendar-checkbox-round" value="' +
                    value.id +
                    '" checked>' +
                    '<span style="border-color: ' +
                    value.borderColor +
                    '; background-color: ' +
                    value.borderColor +
                    ';"></span>' +
                    '<span>' +
                    value.name +
                    '</span>' +
                    '</label></div>'
                );
            });
        calendarList.innerHTML = html.join('\n');
    }
    function CalendarInfo() {
        this.id = null;
        this.name = null;
        this.checked = true;
        this.color = null;
        this.bgColor = null;
        this.borderColor = null;
        this.dragBgColor = null;
    }
    function addCalendar(calendar) {
        CalendarList.push(calendar);
    }
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
});