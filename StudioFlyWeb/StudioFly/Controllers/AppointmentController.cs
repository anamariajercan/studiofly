﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Services;

namespace StudioFly.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentManagementService _appointmentManagement;
        private readonly ILogger _logger;

        public AppointmentController(IAppointmentManagementService appointmentManagement, ILogger<AppointmentController> logger)
        {
            _appointmentManagement = appointmentManagement;
            _logger = logger;
        }

        /// <summary>
        /// Creates a new appointment
        /// </summary>
        /// <param name="model">The <see cref="CreateAppointmentModel">appointment details</see> to be created.</param>
        /// <response code="201">The newly created <see cref="AppointmentDefinition">appointment properties model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="412">Folder not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("createAppointment")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AppointmentDefinition), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateAppointment([FromBody] CreateAppointmentModel model)
        {
            try
            {
                var created = await _appointmentManagement.CreateAppointmentAsync(model).ConfigureAwait(false);
                return LogAndReturnCreated(new Uri($"/appointments/{created.Id}", UriKind.Relative), created);
            }
            catch (Exception ex) when (ex is ValidationException || ex is ArgumentException)
            {
                return LogAndReturnBadRequest($"Request parameters missing or invalid. {ex.Message}");
            }
            catch (UnauthorizedAccessException)
            {
                return LogAndReturnForbidden("User does not have write permission to the specified folder.");
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerError(ex, $"Failed to create user: {model.ClientId}; {ex.Message}");
            }
        }

        /// <summary>
        /// Creates a new appointment
        /// </summary>
        /// <param name="model">The <see cref="CreateAppointmentModel">appointment details</see> to be created.</param>
        /// <response code="201">The newly created <see cref="AppointmentDefinition">appointment properties model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="412">Folder not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("editAppointment")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AppointmentDefinition), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EditAppointment([FromBody] CreateAppointmentModel model)
        {
            try
            {
                var appointment = await _appointmentManagement.EditAppointmentAsync(model).ConfigureAwait(false);
                return LogAndReturnOk(appointment);
            }
            catch (Exception ex) when (ex is ValidationException || ex is ArgumentException)
            {
                return LogAndReturnBadRequest($"Request parameters missing or invalid. {ex.Message}");
            }
            catch (UnauthorizedAccessException)
            {
                return LogAndReturnForbidden("User does not have write permission to the specified folder.");
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerError(ex, $"Failed to create user: {model.ClientId}; {ex.Message}");
            }
        }

        /// <summary>
        /// Creates a new appointment
        /// </summary>
        /// <param name="id">The  Id of the appointment to be deleted.</param>
        /// <response code="201">The newly created <see cref="AppointmentDefinition">appointment properties model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="412">Folder not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpDelete("deleteAppointment/{id}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteAppointment(int id)
        {
            try
            {
                await _appointmentManagement.DeleteAppointmentAsync(id).ConfigureAwait(false);
                return Ok();
            }
            catch (Exception ex) when (ex is ValidationException || ex is ArgumentException)
            {
                return LogAndReturnBadRequest($"Request parameters missing or invalid. {ex.Message}");
            }
            catch (UnauthorizedAccessException)
            {
                return LogAndReturnForbidden("User does not have write permission to the specified folder.");
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerError(ex, $"Failed to delete app: {id}; {ex.Message}");
            }
        }

        /// <summary>
        /// Example endpoint that returns a list of appointments
        /// </summary>
        /// <returns>A <see cref="AppointmentModel"/> object containing hello content.</returns>
        [HttpGet("appointments")]
        [ProducesResponseType(typeof(List<AppointmentModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAppointmentsAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointments = await _appointmentManagement.GetAppointmentsAsync().ConfigureAwait(false);
                return Ok(appointments);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns a list of appointments
        /// </summary>
        /// <returns>A <see cref="AppointmentModel"/> object containing hello content.</returns>
        [HttpGet("appointments/{id}")]
        [ProducesResponseType(typeof(List<AppointmentModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAppointmentBasedOnIdAsync(int id)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointments = await _appointmentManagement.GetAppointmentBasedOnIdAsync(id).ConfigureAwait(false);
                return Ok(appointments);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns a list of features
        /// </summary>
        /// <returns>A <see cref="FeatureModel"/> object containing hello content.</returns>
        [HttpGet("features/{appointmentTypeName}")]
        [ProducesResponseType(typeof(List<FeatureModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFeaturesAsync(string appointmentTypeName)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var features = await _appointmentManagement.GetFeaturesAsync(appointmentTypeName).ConfigureAwait(false);
                return Ok(features);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns a list of features
        /// </summary>
        /// <returns>A <see cref="AppointmentTypeModel"/> object containing hello content.</returns>
        [HttpGet("appointmentTypes")]
        [ProducesResponseType(typeof(List<AppointmentTypeModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAppointmentTypesAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointmentTypes = await _appointmentManagement.GetAppointmentTypesAsync().ConfigureAwait(false);
                return Ok(appointmentTypes);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }
        /// <summary>
        /// Example endpoint that returns a list of features appointment types
        /// </summary>
        /// <returns>A <see cref="FeatureAppointmentTypeModel"/> object containing hello content.</returns>
        [HttpGet("featureAppointmentTypes")]
        [ProducesResponseType(typeof(List<AppointmentTypeModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFeatureAppointmentTypesAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var featureAppointmentTypes = await _appointmentManagement.GetFeatureAppointmentTypes().ConfigureAwait(false);
                return Ok(featureAppointmentTypes);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }
        /// <summary>
        /// Example endpoint that returns a list of features
        /// </summary>
        /// <returns>A <see cref="AppointmentTypeModel"/> object containing hello content.</returns>
        [HttpGet("appointmentTypes/{appointmentTypeName}")]
        [ProducesResponseType(typeof(List<AppointmentTypeModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAppointmentTypesAsync(string appointmentTypeName)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointmentTypes = await _appointmentManagement.GetAppointmentTypesAsync(appointmentTypeName).ConfigureAwait(false);
                return Ok(appointmentTypes);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        private OkObjectResult LogAndReturnOk<T>(T result) where T : class
        {
            return Ok(result);
        }

        private CreatedResult LogAndReturnCreated<T>(Uri uri, T result)
        {
            return Created(uri, result);
        }

        private BadRequestObjectResult LogAndReturnBadRequest(string errMessage)
        {
            return BadRequest(errMessage);
        }

        private ObjectResult LogAndReturnForbidden(string errMessage)
        {
            return StatusCode((int)HttpStatusCode.Forbidden, errMessage);
        }

        private ObjectResult LogAndReturnInternalServerError(Exception exception, string errMessage)
        {
            return StatusCode((int)HttpStatusCode.InternalServerError, errMessage);
        }

        private void ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid parameters");
            }
        }

        private IActionResult LogAndReturnUnauthorizedStatus(Exception ex, string message)
        {
            _logger.LogError(ex, message);
            return Unauthorized();
        }

        private IActionResult LogAndReturnBadRequestStatus(Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            ModelState.AddModelError("123", ex.Message);
            return BadRequest(ModelState);
        }

        private IActionResult LogAndReturnInternalServerErrorStatus(Exception exception, string errorMessage)
        {
            _logger.LogCritical(exception, $"{errorMessage}");
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}