﻿using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models;
using StudioFly.Adapters.Services;
using StudioFly.Adapters.ViewModels;

namespace StudioFly.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserManagementService _userManagement;
        private readonly IAppointmentManagementService _appointmentManagementService;
        private readonly ILogger _logger;

        public HomeController(IAppointmentManagementService appointmentManagementService, IUserManagementService userManagement, ILogger<UsersController> logger)
        {
            _userManagement = userManagement;
            _appointmentManagementService = appointmentManagementService;
            _logger = logger;
        }
        public IActionResult Index()
        {

            var indent = (ClaimsIdentity)User.Identity;
            var userId = indent.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;
            var user = (_userManagement.GetUsersAsync().Result).FirstOrDefault(m => m.Id.ToString() == userId);
            var createAppointmentsViewModel = new CreateAppointmentsViewModel()
            {
                LoggedInUserModel = user
            };
            return View(createAppointmentsViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Blog()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        public IActionResult Login()
        {
            return Challenge(new AuthenticationProperties() { RedirectUri = "Home" },
                OpenIdConnectDefaults.AuthenticationScheme);
        }

        public IActionResult Logoff()
        {
            return SignOut(new AuthenticationProperties() { RedirectUri = "Home" },
                OpenIdConnectDefaults.AuthenticationScheme,
                CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
