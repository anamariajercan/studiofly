﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.User;
using StudioFly.Adapters.Services;

namespace StudioFly.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserManagementService _userManagement;
        private readonly ILogger _logger;

        public UsersController(IUserManagementService userManagement, ILogger<UsersController> logger)
        {
            _userManagement = userManagement;
            _logger = logger;
        }

        /// <summary>
        /// Example endpoint that returns string users
        /// </summary>
        /// <returns>A <see cref="UserModel"/> object containing hello content.</returns>
        [HttpGet("users")]
        [ProducesResponseType(typeof(List<UserModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUsersAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var users = await _userManagement.GetUsersAsync().ConfigureAwait(false);
                return Ok(users);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns string users
        /// </summary>
        /// <returns>A <see cref="UserModel"/> object containing hello content.</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUser(int id)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var users = await _userManagement.GetUserById(id).ConfigureAwait(false);
                return Ok(users);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns string users
        /// </summary>
        /// <returns>A <see cref="ClientModel"/> object containing hello content.</returns>
        [HttpGet("clients")]
        [ProducesResponseType(typeof(List<ClientModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetClientsAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var clients = await _userManagement.GetClientsAsync().ConfigureAwait(false);
                return Ok(clients);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetClientsAsync failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetClientsAsync failed");
            }
        }

        /// <summary>
        /// Example endpoint that returns string users
        /// </summary>
        /// <returns>A <see cref="EmployeeModel"/> object containing hello content.</returns>
        [HttpGet("employees")]
        [ProducesResponseType(typeof(List<UserModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var users = await _userManagement.GetEmployeesAsync().ConfigureAwait(false);
                return Ok(users);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetUsers failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetUsers failed");
            }
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="model">The <see cref="UserModel">user details</see> to be created.</param>
        /// <response code="201">The newly created <see cref="UserModel">User model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("createUser")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserModel model)
        {

            try
            {
                var created = await _userManagement.CreateUserAsync(model).ConfigureAwait(false);
                return LogAndReturnCreated(new Uri($"/users/{created.Id}", UriKind.Relative), created);
            }
            catch (Exception ex) when (ex is ValidationException || ex is ArgumentException)
            {
                return LogAndReturnBadRequest($"Request parameters missing or invalid. {ex.Message}");
            }
            catch (UnauthorizedAccessException)
            {
                return LogAndReturnForbidden("User does not have write permission to the specified folder.");
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerError(ex, $"Failed to create user: {model.Username}; {ex.Message}");
            }
        }

        private CreatedResult LogAndReturnCreated<T>(Uri uri, T result)
        {
            return Created(uri, result);
        }

        private BadRequestObjectResult LogAndReturnBadRequest(string errMessage)
        {
            return BadRequest(errMessage);
        }

        private ObjectResult LogAndReturnForbidden(string errMessage)
        {
            return StatusCode((int)HttpStatusCode.Forbidden, errMessage);
        }

        private ObjectResult LogAndReturnInternalServerError(Exception exception, string errMessage)
        {
            return StatusCode((int)HttpStatusCode.InternalServerError, errMessage);
        }

        private void ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid parameters");
            }
        }

        private IActionResult LogAndReturnUnauthorizedStatus(Exception ex, string message)
        {
            _logger.LogError(ex, message);
            return Unauthorized();
        }

        private IActionResult LogAndReturnBadRequestStatus(Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            ModelState.AddModelError("123", ex.Message);
            return BadRequest(ModelState);
        }

        private IActionResult LogAndReturnInternalServerErrorStatus(Exception exception, string errorMessage)
        {
            _logger.LogCritical(exception, $"{errorMessage}");
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }

        public IActionResult Users()
        {
            return View();
        }
    }
}