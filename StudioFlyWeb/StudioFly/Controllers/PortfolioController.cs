﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using StudioFly.Adapters.Models.User;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudioFly.Adapters.Services;

namespace StudioFly.Controllers
{
    public class PortfolioController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IUserManagementService _userManagement;

        public PortfolioController(IUserManagementService userManagement, IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _userManagement = userManagement;
        }

        public async Task<IActionResult> Portfolio()
        {
            return View(await GetAllEmployees().ConfigureAwait(false));
        }

        public async Task<IActionResult> Employee(string employeeName)
        {
            EmployeeModel model = await GetSelectedEmployeeModel(employeeName).ConfigureAwait(false);
            return View(model);
        }

        public async Task<EmployeeModel> GetSelectedEmployeeModel(string name)
        {
            var employees =  await GetAllEmployees().ConfigureAwait(false);
            var employee = employees.FirstOrDefault(x => x.User.Username == name);
            return employee;
        }

        public async Task<List<EmployeeModel>> GetAllEmployees()
        {
            var employees = await _userManagement.GetEmployeesAsync().ConfigureAwait(false);
            return employees.ToList();
        }
    }
}