﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Models.User;
using StudioFly.Adapters.Services;
using StudioFly.Adapters.ViewModels;

namespace StudioFly.Controllers
{
    [Authorize]
    public class AppointmentsController : Controller
    {
        private readonly IUserManagementService _userManagement;
        private readonly IAppointmentManagementService _appointmentManagementService;
        private readonly ILogger _logger;

        public AppointmentsController(IAppointmentManagementService appointmentManagementService, IUserManagementService userManagement, ILogger<UsersController> logger)
        {
            _userManagement = userManagement;
            _appointmentManagementService = appointmentManagementService;
            _logger = logger;
        }
        public IActionResult TeamAppointments()
        {
            var indent = (ClaimsIdentity)User.Identity;
            var userId = indent.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;
            var user=(_userManagement.GetUsersAsync().Result).FirstOrDefault(m=>m.Id.ToString()==userId);
            IEnumerable<EmployeeModel> employees = _userManagement.GetEmployeesAsync().Result;
            IEnumerable<ClientModel> clients = _userManagement.GetClientsAsync().Result;
            IEnumerable<FeatureAppointmentTypeModel>
                featureAppointmentTypes = _appointmentManagementService.GetFeatureAppointmentTypes().Result;
            
            var createAppointmentsViewModel = new CreateAppointmentsViewModel()
            {
                FeatureAppointmentTypeModel = featureAppointmentTypes,
                EmployeeModels = employees,
                ClientModels = clients,
                LoggedInUserModel = user
            };

            return View(createAppointmentsViewModel);
        }
    }
}