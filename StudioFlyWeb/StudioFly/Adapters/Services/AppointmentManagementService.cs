﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Providers.Appointments;

namespace StudioFly.Adapters.Services
{
    public class AppointmentManagementService : IAppointmentManagementService
    {
        private readonly IAppointmentProvider _appointmentProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentManagementService"/> class.
        /// </summary>
        /// <param name="appointmentProvider"></param>
        public AppointmentManagementService(IAppointmentProvider appointmentProvider)
        {
            _appointmentProvider = appointmentProvider;
        }

        public async Task<AppointmentDefinition> CreateAppointmentAsync(CreateAppointmentModel model)
        {
            AppointmentDefinition retValue;
            try
            {
                retValue = await _appointmentProvider.CreateAppointmentAsync(model).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<List<AppointmentDefinition>> GetAppointmentsAsync()
        {
            List<AppointmentDefinition> retValue;
            try
            {
                retValue = await _appointmentProvider.GetAppointments().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }
            
            return retValue;
        }

        public async Task<List<FeatureModel>> GetFeaturesAsync(string appointmentTypeName)
        {
            List<FeatureModel> retValue;
            try
            {
                retValue = await _appointmentProvider.GetFeatures(appointmentTypeName).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            return retValue;
        }

        public async Task<List<AppointmentTypeModel>> GetAppointmentTypesAsync(string appointmentTypeName)
        {
            List<AppointmentTypeModel> retValue;
            try
            {
                retValue = await _appointmentProvider.GetAppointmentTypes(appointmentTypeName).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            return retValue;
        }

        public async Task<List<AppointmentTypeModel>> GetAppointmentTypesAsync()
        {
            List<AppointmentTypeModel> retValue;
            try
            {
                retValue = await _appointmentProvider.GetAppointmentTypes().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            return retValue;
        }

        public async Task DeleteAppointmentAsync(int id)
        {
            try
            {
                await _appointmentProvider.DeleteAppointment(id).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }
        }

        public async Task<AppointmentDefinition> EditAppointmentAsync(CreateAppointmentModel model)
        {
            AppointmentDefinition retValue;
            try
            {
                retValue = await _appointmentProvider.EditAppointmentAsync(model).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<AppointmentDefinition> GetAppointmentBasedOnIdAsync(int id)
        {
            AppointmentDefinition retValue;
            try
            {
                retValue = await _appointmentProvider.GetAppointmentBasedOnIdAsync(id).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            return retValue;
        }

        public async Task<List<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes()
        {
            List<FeatureAppointmentTypeModel> retValue;
            try
            {
                retValue = await _appointmentProvider.GetFeatureAppointmentTypes().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            return retValue;
        }
    }
}
