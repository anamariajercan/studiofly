﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.User;
using StudioFly.Adapters.Providers.User;

namespace StudioFly.Adapters.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly IUserProvider _usersProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagementService"/> class.
        /// </summary>
        /// <param name="usersProvider"></param>
        public UserManagementService(IUserProvider usersProvider)
        {
            _usersProvider = usersProvider;
        }

        public async Task<UserModel> CreateUserAsync(CreateUserModel model)
        {
            UserModel retValue;
            try
            {
                retValue = await _usersProvider.CreateUserAsync(model).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolio()
        {
            IEnumerable<EmployeePortfolio> retValue;
            try
            {
                retValue = await _usersProvider.GetEmployeesPortfolioAsync().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<IEnumerable<ClientModel>> GetClientsAsync()
        {
            IEnumerable<ClientModel> retValue;
            try
            {
                retValue = await _usersProvider.GetClientsAsync().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        /// <summary>
        /// Gets the list of users from the user management database
        /// </summary>
        /// <returns>An instance of list of UserModel </returns>
        public async Task<IEnumerable<UserModel>> GetUsersAsync()
        {
            IEnumerable<UserModel> retValue;
            try
            {
                retValue = await _usersProvider.GetUsersAsync().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException( ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<UserModel> GetUserById(int id)
        {
            UserModel retValue;
            try
            {
                retValue = await _usersProvider.GetUserBasedOnId(id).ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }

        public async Task<IEnumerable<EmployeeModel>> GetEmployeesAsync()
        {
            IEnumerable<EmployeeModel> retValue;
            try
            {
                retValue = await _usersProvider.GetEmployeesAsync().ConfigureAwait(false);
            }
            catch (ProviderException ex)
            {
                throw new UserException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new UserException(ex.Message, ex);
            }

            if (retValue == null)
            {
                throw new UserException(400, "Guid does not exist");
            }
            return retValue;
        }
    }
}
