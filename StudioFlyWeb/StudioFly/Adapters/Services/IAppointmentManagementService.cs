﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.Appointments;

namespace StudioFly.Adapters.Services
{
    public interface IAppointmentManagementService
    {
        Task<AppointmentDefinition> CreateAppointmentAsync(CreateAppointmentModel model);
        Task<List<AppointmentDefinition>> GetAppointmentsAsync();
        Task<List<FeatureModel>> GetFeaturesAsync(string appointmentTypeName);
        Task<List<AppointmentTypeModel>> GetAppointmentTypesAsync(string appointmentTypeName);
        Task<List<AppointmentTypeModel>> GetAppointmentTypesAsync();
        Task DeleteAppointmentAsync(int id);
        Task<AppointmentDefinition> EditAppointmentAsync(CreateAppointmentModel model);
        Task<AppointmentDefinition> GetAppointmentBasedOnIdAsync(int id);
        Task<List<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes();
    }
}