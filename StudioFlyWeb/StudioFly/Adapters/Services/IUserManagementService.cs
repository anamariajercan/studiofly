﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Services
{
    public interface IUserManagementService
    {
        /// <summary>
        /// Gets the users from user management service
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<UserModel>> GetUsersAsync();
        Task<UserModel> GetUserById(int id);

        /// <summary>
        /// Gets the employees from user management service
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EmployeeModel>> GetEmployeesAsync();

        /// <summary>
        /// Creates an user in the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<UserModel> CreateUserAsync(CreateUserModel model);

        /// <summary>
        /// Creates an user in the database
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolio();
        Task<IEnumerable<ClientModel>> GetClientsAsync();
    }
}
