﻿using System.Collections.Generic;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.ViewModels
{
    public class CreateAppointmentsViewModel
    {
        public IEnumerable<AccessoryModel> AccessoryModels { get; set; }
        public IEnumerable<FeatureAppointmentTypeModel> FeatureAppointmentTypeModel { get; set; }
        public IEnumerable<EmployeeModel> EmployeeModels { get; set; }
        public IEnumerable<ClientModel> ClientModels { get; set; }
        public UserModel LoggedInUserModel { get; set; }
    }
}
