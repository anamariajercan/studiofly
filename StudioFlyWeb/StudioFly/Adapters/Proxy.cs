﻿using System;
using System.Configuration.Provider;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace StudioFly.Adapters
{
    /// <summary>
    /// Interface to Autonomous Services
    /// </summary>
    public class Proxy
    {
        private readonly ILogger _logger;
        private readonly HttpClient _httpClient;
        private readonly Uri _serviceBaseAddress;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="httpClient"></param>
        /// <param name="accessor"></param>
        /// <param name="serviceBaseAddress"></param>
        public Proxy(ILogger logger, HttpClient httpClient, IHttpContextAccessor accessor, Uri serviceBaseAddress)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((sender, cert, chain, errors) => cert.Subject.Contains("waters_connect"));
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

            _logger = logger;
            _httpClient = httpClient;
            _serviceBaseAddress = serviceBaseAddress;
            _httpClient.CopyAuthHeader(accessor);
            _httpContextAccessor = accessor;
        }

        /// <summary>
        /// Generic operation from the Uri
        /// </summary>
        /// <typeparam name="TReturn"></typeparam>
        /// <param name="methodType"></param>
        /// <param name="uri"></param>
        /// <param name="bodyParams"></param>
        /// <returns></returns>
        public async Task<TReturn> Send<TReturn>(HttpMethod methodType, Uri uri, object bodyParams = null)
        {
            try
            {
                var idToken = await AuthenticationHttpContextExtensions.GetTokenAsync(_httpContextAccessor.HttpContext, "id_token");
                var accessToken = await AuthenticationHttpContextExtensions.GetTokenAsync(_httpContextAccessor.HttpContext, "access_token");
                _httpClient.SetBearerToken(idToken);
                _logger.LogDebug($"{methodType} [{uri}] requested");
                var requestMessage = new HttpRequestMessage(methodType, new Uri(_serviceBaseAddress, uri));
                if (bodyParams != null)
                {
                    requestMessage.Content = new StringContent(JsonConvert.SerializeObject(bodyParams), Encoding.UTF8, "application/json");
                }
                using (var response = await _httpClient.SendAsync(requestMessage).ConfigureAwait(false))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonStr = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return ToObject<TReturn>(jsonStr);
                    }
                    var parsed = await TryParse(response).ConfigureAwait(false);
                    var details = $"{response.ReasonPhrase} => [[{parsed}]]";
                    _logger.LogError($"ProxyAS Http Request Failed to return. Status code: {response.StatusCode}");
                    _logger.LogError(details);
                    throw new ProviderException(details);
                }
            }
            catch (HttpRequestException ex)
            {
                var details = $"ProxyAS Http Request Error {ex.Message} => [[{ex.InnerException?.Message}]]  => [[{ex.InnerException?.InnerException?.Message}]]";
                _logger.LogError(ex, details);
                throw new ProviderException(details, ex);
            }
        }

        private async Task<string> TryParse(HttpResponseMessage response)
        {
            try
            {
                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
            catch (Exception)
            {
                return "";
            }
        }

        protected TReturn ToObject<TReturn>(string jsonStr)
        {
            try
            {
                return JsonConvert.DeserializeObject<TReturn>(jsonStr);
            }
            catch (JsonSerializationException ex)
            {
                _logger.LogError(ex, "ToObject Error {message}", ex.Message);
                throw new ProviderException(ex.Message, ex);
            }
        }
    }
}
