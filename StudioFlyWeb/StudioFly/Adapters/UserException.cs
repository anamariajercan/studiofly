﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace StudioFly.Adapters
{
    /// <summary>
    /// Exception raised by the application Core
    /// </summary>
    [Serializable]
    public class UserException : Exception
    {
        public int StatusCode { get; }

        public UserException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public UserException() : this(500)
        {
        }

        public UserException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public UserException(int statusCode, string message, Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }

        public UserException(string message) : this(500, message)
        {
        }

        public UserException(string message, Exception innerException) : this(500, message, innerException)
        {
        }

        protected UserException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            StatusCode = serializationInfo.GetInt32("StatusCode");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }
            info.AddValue("StatusCode", StatusCode);
            base.GetObjectData(info, context);
        }
    }
}
