﻿namespace StudioFly.Adapters.Models.User
{
    public class EmployeePortfolio
    {
        public int Id { get; set; }
        public string PortfolioPath { get; set; }
        public int EmployeeId { get; set; }
        public EmployeeModel Employee { get; set; }
    }
}
