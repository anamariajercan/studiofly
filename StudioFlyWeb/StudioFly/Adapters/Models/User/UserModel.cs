﻿namespace StudioFly.Adapters.Models.User
{
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public int Id { get; set; }
    }
}