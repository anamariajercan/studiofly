﻿using System;
using System.Collections.Generic;

namespace StudioFly.Adapters.Models.User
{
    [Serializable]
    public sealed class EmployeeModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public UserModel User { get; set; }
        public List<EmployeePortfolio> EmployeePortfolio { get; set; }

        //public List<EmployeePortfolio>EmployeePortfolio
        //{
        //    get => _employeePortfolio;
        //    set
        //    {
        //        _employeePortfolio = value;
        //        _employeePortfolio = new List<EmployeePortfolio>();
        //    }
        //}

        private List<EmployeePortfolio> _employeePortfolio;
    }
}
