﻿using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Models.Appointments
{
    public class AppointmentDefinition
    {
        public int Id { get; set; }

        public string AppointmentTimeStart { get; set; }
        public string AppointmentTimeEnd { get; set; }
        public string EmployeeName { get; set; }
        public string ClientName { get; set; }
        public int Price { get; set; }
        public string FeatureName { get; set; }
        public string AppointmentTypeName { get; set; }
        public AppointmentModel AppointmentModel { get; set; }
        public AppointmentTypeModel AppointmentTypeModel { get; set; }
        public ClientModel ClientModel { get; set; }
        public EmployeeModel EmployeeModel { get; set; }
        public FeatureModel FeatureModel { get; set; }
        public int TimeToCompletion { get; set; }
    }
}
