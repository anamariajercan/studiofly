﻿using System.Collections.Generic;

namespace StudioFly.Adapters.Models.Appointments
{
    public class AccessoryModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }
        public ICollection<AppointmentAccessoryModel> AppointmentAccessories { get; set; }
    }
}
