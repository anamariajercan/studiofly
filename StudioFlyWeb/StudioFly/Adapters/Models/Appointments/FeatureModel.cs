﻿
using System.Collections.Generic;

namespace StudioFly.Adapters.Models.Appointments
{
    public class FeatureModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<FeatureAppointmentTypeModel> FeatureAppointmentTypes { get; set; }
    }
}
