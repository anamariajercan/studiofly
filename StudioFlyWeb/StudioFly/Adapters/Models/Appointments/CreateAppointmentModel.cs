﻿using System.Collections.Generic;

namespace StudioFly.Adapters.Models.Appointments
{
    public class CreateAppointmentModel
    {
        public int Id { get; set; }
        public int OriginalId { get; set; }
        public string AppointmentTime { get; set; }
        public int EmployeeId { get; set; }
        public int ClientId { get; set; }
        public int FeatureAppointmentTypeId { get; set; }
        public List<int> AccessoriesId { get; set; }
    }
}
