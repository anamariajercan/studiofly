﻿namespace StudioFly.Adapters.Models.Appointments
{
    public class AppointmentAccessoryModel
    {
        public int Id { get; set; }


        public int AppointmentId { get; set; }
        public AppointmentModel Appointment { get; set; }
        public int AccessoryId { get; set; }
        public AccessoryModel Accessory { get; set; }
    }
}
