﻿using System.Collections.Generic;

namespace StudioFly.Adapters.Models.Appointments
{
    public class AppointmentTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<FeatureAppointmentTypeModel> FeatureAppointmentTypes { get; set; }
    }
}
