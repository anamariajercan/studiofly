﻿using System;
using System.Collections.Generic;

namespace StudioFly.Adapters.Models.Appointments
{
    public class AppointmentModel
    {
        public int Id { get; set; }
        public DateTime AppointmentTime { get; set; }

        public int EmployeeId { get; set; }
        public int ClientId { get; set; }
        public int Price { get; set; }

        public int FeatureAppointmentTypeId { get; set; }
        public FeatureAppointmentTypeModel FeatureAppointmentType { get; set; }
        public ICollection<AppointmentAccessoryModel> AppointmentAccessories { get; set; }
        public int OriginalId { get; set; }
    }
}
