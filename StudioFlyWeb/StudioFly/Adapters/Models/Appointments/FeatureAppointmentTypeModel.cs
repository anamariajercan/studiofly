﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Adapters.Models.Appointments
{
    public class FeatureAppointmentTypeModel
    {
        public int Id { get; set; }
        public AppointmentTypeModel AppointmentType { get; set; }
        public int AppointmentTypeId { get; set; }
        public FeatureModel Feature { get; set; }
        public int FeatureId { get; set; }
        public int Price { get; set; }
        [Required]
        public int TimeToCompleteInMinutes { get; set; }
    }
}
