﻿using System.Collections.Generic;
using System.Linq;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Providers.Appointments
{
    public class AppointmentConverter
    {
        internal static IEnumerable<AppointmentDefinition> ToAppointments(List<AppointmentModel> ret,
            List<ClientModel> clients, List<EmployeeModel> employees, List<FeatureAppointmentTypeModel> featureAppointmentTypeModels)
        {
            var appointmentDefinitions = new List<AppointmentDefinition>();
            foreach (var appointment in ret)
            {
                var client = clients.FirstOrDefault(x => x.Id == appointment.ClientId);
                var employee = employees.FirstOrDefault(x => x.Id == appointment.EmployeeId);
                var featureAppointmentTypeModel = featureAppointmentTypeModels.FirstOrDefault(x => x.Id == appointment.FeatureAppointmentTypeId);
                var appointmentDefinition = ToAppointment(appointment, featureAppointmentTypeModel, client, employee);
                appointmentDefinitions.Add(appointmentDefinition);
            }
            return appointmentDefinitions;
        }

        public static AppointmentDefinition ToAppointment(AppointmentModel appointmentModel, FeatureAppointmentTypeModel featureAppointmentTypeModel, ClientModel client, EmployeeModel employee)
        {
            if (appointmentModel == null) return null;
            return new AppointmentDefinition
            {
                AppointmentTimeStart = appointmentModel.AppointmentTime.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'"),
                AppointmentTimeEnd = appointmentModel.AppointmentTime.AddMinutes(double.Parse(featureAppointmentTypeModel.TimeToCompleteInMinutes.ToString())).ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'"),
                AppointmentModel = appointmentModel,
                AppointmentTypeModel = featureAppointmentTypeModel.AppointmentType,
                EmployeeModel = employee,
                ClientModel = client,
                FeatureModel = featureAppointmentTypeModel.Feature,
                TimeToCompletion = featureAppointmentTypeModel.TimeToCompleteInMinutes,
            };
        }
    }
}
