﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.Appointments;

namespace StudioFly.Adapters.Providers.Appointments.ProxyAppointment
{
    public interface IAppointmentProxy
    {
        Task<AppointmentDefinition> CreateAppointment(CreateAppointmentModel model);
        Task<IEnumerable<AppointmentModel>> GetAppointments();
        Task<IEnumerable<FeatureModel>> GetFeatures(string appointmentTypeName);
        Task<IEnumerable<FeatureModel>> GetFeatures();

        Task<IEnumerable<AppointmentTypeModel>> GetAppointmentTypes(string appointmentTypeName);
        Task<IEnumerable<AppointmentTypeModel>> GetAppointmentTypes();
        Task DeleteAppointment(int id);
        Task<AppointmentDefinition> EditAppointment(CreateAppointmentModel model);
        Task<IEnumerable<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes();
    }
}