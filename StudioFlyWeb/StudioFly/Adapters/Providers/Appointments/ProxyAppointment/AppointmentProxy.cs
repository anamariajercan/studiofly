﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models.Appointments;

namespace StudioFly.Adapters.Providers.Appointments.ProxyAppointment
{
    public class AppointmentProxy : Proxy, IAppointmentProxy
    {
        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="httpClient"></param>
        /// <param name="settings"></param>
        /// <param name="accessor"></param>
        public AppointmentProxy(ILogger<Proxy> logger, HttpClient httpClient, AppointmentSettings settings, IHttpContextAccessor accessor)
            : base(logger, httpClient, accessor, new Uri(settings.ServiceBaseAddress, "v1.0/"))
        {
        }

        public async Task<AppointmentDefinition> CreateAppointment(CreateAppointmentModel model)
        {
            var uri = new Uri("appointments/create/appointment", UriKind.Relative);
            return await Send<AppointmentDefinition>(HttpMethod.Post, uri, model).ConfigureAwait(false);

        }

        public async Task<IEnumerable<AppointmentModel>> GetAppointments()
        {
            var uri = new Uri("appointments/appointments", UriKind.Relative);
            return await Send<List<AppointmentModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<FeatureModel>> GetFeatures()
        {
            var uri = new Uri("appointments/features", UriKind.Relative);
            return await Send<List<FeatureModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<FeatureModel>> GetFeatures(string appointmentTypeName)
        {
            var uri = new Uri($"appointments/features/{appointmentTypeName}", UriKind.Relative);
            return await Send<List<FeatureModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<AppointmentTypeModel>> GetAppointmentTypes(string appointmentTypeName)
        {
            var uri = new Uri($"appointments/appointmentTypes/{appointmentTypeName}", UriKind.Relative);
            return await Send<List<AppointmentTypeModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<AppointmentTypeModel>> GetAppointmentTypes()
        {
            var uri = new Uri($"appointments/appointmentTypes", UriKind.Relative);
            return await Send<List<AppointmentTypeModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task DeleteAppointment(int id)
        {
            var uri = new Uri($"appointments/delete/{id}", UriKind.Relative);
            await Send<bool>(HttpMethod.Delete, uri).ConfigureAwait(false);
        }

        public async Task<AppointmentDefinition> EditAppointment(CreateAppointmentModel model)
        {
            var uri = new Uri("appointments/edit/appointment", UriKind.Relative);
            return await Send<AppointmentDefinition>(HttpMethod.Post, uri, model).ConfigureAwait(false);
        }

        public async Task<IEnumerable<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes()
        {
            var uri = new Uri($"appointments/featureAppointmentTypes", UriKind.Relative);
            return await Send<List<FeatureAppointmentTypeModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }
    }
}
