﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models.Appointments;
using StudioFly.Adapters.Providers.Appointments.ProxyAppointment;
using StudioFly.Adapters.Providers.User.ProxyUser;

namespace StudioFly.Adapters.Providers.Appointments
{
    public class AppointmentProvider: IAppointmentProvider
    {
        private readonly ILogger _logger;
        private readonly IAppointmentProxy _appointmentProxy;
        private readonly IUserProxy _userProxy;

        public AppointmentProvider(ILogger<AppointmentProvider> logger, IAppointmentProxy appointmentProxy, IUserProxy userProxy)
        {
            _appointmentProxy = appointmentProxy;
            _userProxy = userProxy;
            _logger = logger;
        }

        public async Task<AppointmentDefinition> CreateAppointmentAsync(CreateAppointmentModel model)
        {
            return await _appointmentProxy.CreateAppointment(model).ConfigureAwait(false);
        }

        public async Task<List<AppointmentDefinition>> GetAppointments()
        {
            var appointments= await _appointmentProxy.GetAppointments().ConfigureAwait(false);
            var clients = await _userProxy.GetClientsModel().ConfigureAwait(false);
            var employees = await _userProxy.GetEmployees().ConfigureAwait(false);
            var featureAppointmentTypes = await _appointmentProxy.GetFeatureAppointmentTypes().ConfigureAwait(false);
            return AppointmentConverter.ToAppointments(appointments.ToList(), clients.ToList(), employees.ToList(), featureAppointmentTypes.ToList()).ToList();
        }

        public async Task<List<FeatureModel>> GetFeatures(string appointmentTypeName)
        {
            return (await _appointmentProxy.GetFeatures(appointmentTypeName).ConfigureAwait(false)).ToList();
        }

        public async Task<List<FeatureModel>> GetFeatures()
        {
            return (await _appointmentProxy.GetFeatures().ConfigureAwait(false)).ToList();
        }

        public async Task<List<AppointmentTypeModel>> GetAppointmentTypes(string appointmentTypeName)
        {
            return (await _appointmentProxy.GetAppointmentTypes(appointmentTypeName).ConfigureAwait(false)).ToList();

        }

        public async Task<List<AppointmentTypeModel>> GetAppointmentTypes()
        {
            return (await _appointmentProxy.GetAppointmentTypes().ConfigureAwait(false)).ToList();

        }

        public async Task DeleteAppointment(int id)
        {
            await _appointmentProxy.DeleteAppointment(id).ConfigureAwait(false);
        }

        public async Task<AppointmentDefinition> EditAppointmentAsync(CreateAppointmentModel model)
        {
            return await _appointmentProxy.EditAppointment(model).ConfigureAwait(false);
        }

        public async Task<AppointmentDefinition> GetAppointmentBasedOnIdAsync(int id)
        {
            var appointments = await GetAppointments().ConfigureAwait(false);
            return appointments.FirstOrDefault(x => x.AppointmentModel.Id == id);
        }

        public async Task<List<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes()
        {
            return (await _appointmentProxy.GetFeatureAppointmentTypes().ConfigureAwait(false)).ToList();
        }
    }
}