﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.Appointments;

namespace StudioFly.Adapters.Providers.Appointments
{
    public interface IAppointmentProvider
    {

        /// <summary>
        /// Creates an user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<AppointmentDefinition> CreateAppointmentAsync(CreateAppointmentModel model);
        Task<List<AppointmentDefinition>> GetAppointments();
        Task<List<FeatureModel>> GetFeatures(string appointmentTypeName);
        Task<List<AppointmentTypeModel>> GetAppointmentTypes(string appointmentTypeName);
        Task<List<AppointmentTypeModel>> GetAppointmentTypes();
        Task DeleteAppointment(int id);
        Task<AppointmentDefinition> EditAppointmentAsync(CreateAppointmentModel model);
        Task<AppointmentDefinition> GetAppointmentBasedOnIdAsync(int id);
        Task<List<FeatureAppointmentTypeModel>> GetFeatureAppointmentTypes();
    }
}
