﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Providers.User
{
    public interface IUserProvider
    {
        /// <summary>
        /// Get a list of Users
        /// </summary>
        /// <returns>List&lt;Method&gt;</returns>
        Task<IEnumerable<UserModel>> GetUsersAsync();

        /// <summary>
        /// Get a list of Users
        /// </summary>
        /// <returns>List&lt;Method&gt;</returns>
        Task<IEnumerable<EmployeeModel>> GetEmployeesAsync();

        /// <summary>
        /// Creates an user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<UserModel> CreateUserAsync(CreateUserModel model);

        /// <summary>
        /// Gets all employeesPortfolio
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolioAsync();
        Task<IEnumerable<ClientModel>> GetClientsAsync();
        Task<UserModel> GetUserBasedOnId(int id);
    }
}
