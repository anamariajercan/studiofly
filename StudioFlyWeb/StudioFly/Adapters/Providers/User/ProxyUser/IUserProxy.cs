﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Providers.User.ProxyUser
{
    public interface IUserProxy
    {
        Task<IEnumerable<UserModel>> GetUsers();
        Task<IEnumerable<EmployeeModel>> GetEmployees();
        Task<UserModel> CreateUser(CreateUserModel model);
        Task<IEnumerable<UserModel>> GetClients();
        Task<IEnumerable<ClientModel>> GetClientsModel();

        Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolio();
        Task<UserModel> GetUserBasedOnId(int id);
    }
}
