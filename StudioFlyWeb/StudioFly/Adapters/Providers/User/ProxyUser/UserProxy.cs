﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models.User;

namespace StudioFly.Adapters.Providers.User.ProxyUser
{
    public class UserProxy : Proxy, IUserProxy
    {
        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="httpClient"></param>
        /// <param name="settings"></param>
        /// <param name="accessor"></param>
        public UserProxy(ILogger<Proxy> logger, HttpClient httpClient, UserSettings settings, IHttpContextAccessor accessor)
            : base(logger, httpClient, accessor, new Uri(settings.ServiceBaseAddress, "v1.0/"))
        {
        }

        public async Task<IEnumerable<EmployeeModel>> GetEmployees()
        {
            var uri = new Uri($"user/employees", UriKind.Relative);
            return await Send<IEnumerable<EmployeeModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<UserModel> CreateUser(CreateUserModel model)
        {
            var uri = new Uri("user/create", UriKind.Relative);
            return await Send<UserModel>(HttpMethod.Post, uri, model).ConfigureAwait(false);
        }

        public async Task<IEnumerable<UserModel>> GetClients()
        {
            var uri = new Uri($"user/clients", UriKind.Relative);
            return await Send<IEnumerable<UserModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<ClientModel>> GetClientsModel()
        {
            var uri = new Uri($"user/clients", UriKind.Relative);
            return await Send<IEnumerable<ClientModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolio()
        {
            var uri = new Uri($"user/employeesPortfolio", UriKind.Relative);
            return await Send<IEnumerable<EmployeePortfolio>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<UserModel> GetUserBasedOnId(int id)
        {
            var uri = new Uri($"user/{id}", UriKind.Relative);
            return await Send<UserModel>(HttpMethod.Get, uri).ConfigureAwait(false);
        }

        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            var uri = new Uri($"user/users", UriKind.Relative);
            return await Send<IEnumerable<UserModel>>(HttpMethod.Get, uri).ConfigureAwait(false);
        }
    }
}
