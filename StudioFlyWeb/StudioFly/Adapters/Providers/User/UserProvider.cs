﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StudioFly.Adapters.Models.User;
using StudioFly.Adapters.Providers.User.ProxyUser;

namespace StudioFly.Adapters.Providers.User
{
    public class UserProvider: IUserProvider
    {
        private readonly ILogger _logger;
        private readonly IUserProxy _userProxy;

        public UserProvider(ILogger<UserProvider> logger, IUserProxy userProxy)
        {
            _userProxy = userProxy;
            _logger = logger;
        }

        public async Task<IEnumerable<UserModel>> GetUsersAsync()
        {
            return await _userProxy.GetUsers().ConfigureAwait(false);
        }

        public async Task<UserModel> GetUserBasedOnId(int id)
        {
            return await _userProxy.GetUserBasedOnId(id).ConfigureAwait(false);
        }

        public async Task<IEnumerable<EmployeeModel>> GetEmployeesAsync()
        {
            return await _userProxy.GetEmployees().ConfigureAwait(false);
        }

        public async Task<UserModel> CreateUserAsync(CreateUserModel model)
        {
            return await _userProxy.CreateUser(model).ConfigureAwait(false);
        }

        public async Task<IEnumerable<EmployeePortfolio>> GetEmployeesPortfolioAsync()
        {
            return await _userProxy.GetEmployeesPortfolio().ConfigureAwait(false);
        }

        public async Task<IEnumerable<ClientModel>> GetClientsAsync()
        {
            return await _userProxy.GetClientsModel().ConfigureAwait(false);
        }
    }
}