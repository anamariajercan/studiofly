﻿using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace StudioFly.Adapters
{
    internal static class HttpClientExtensions
    {
        public static void CopyAuthHeader(this HttpClient httpClient, IHttpContextAccessor accessor)
        {
            var theRequest = accessor?.HttpContext?.Request;
            if (theRequest != null)
            {
                foreach (var header in theRequest.Headers.Where(h => h.Key == "Authorization"))
                {
                    httpClient.DefaultRequestHeaders.Add(header.Key, header.Value.AsEnumerable());
                }
            }
        }
    }
}