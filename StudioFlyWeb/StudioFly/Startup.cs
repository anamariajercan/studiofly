﻿using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.AspNetCore.Http;
using StudioFly.Adapters;
using StudioFly.Adapters.Providers.Appointments;
using StudioFly.Adapters.Providers.Appointments.ProxyAppointment;
using StudioFly.Adapters.Providers.User;
using StudioFly.Adapters.Providers.User.ProxyUser;
using StudioFly.Adapters.Services;


namespace StudioFly
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry();
            services.AddSingleton<IUserManagementService, UserManagementService>();
            services.AddSingleton<IUserProvider, UserProvider>();
            services.AddSingleton(new UserSettings(Configuration["UserManagementUrl"]));
            services.AddHttpContextAccessor();
            services.AddHttpClient<IAppointmentProxy, AppointmentProxy>(httpClient =>
            {
                var serviceProvider = services.BuildServiceProvider();
                // Find the HttpContextAccessor service
                var httpContextAccessor = serviceProvider.GetService<IHttpContextAccessor>();
                // Get the bearer token from the request context (header)
                var bearerToken = httpContextAccessor.HttpContext.Request
                    .Headers["Authorization"]
                    .FirstOrDefault(h => h.StartsWith("bearer ", StringComparison.InvariantCultureIgnoreCase));
                // Add authorization if found
                if (bearerToken != null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", bearerToken);
                //else
                //    httpClient.SetUserCertificateAuthorizationHeader(httpContextAccessor.HttpContext.User);
                httpClient.BaseAddress = new Uri(Configuration["AppointmentManagementUrl"]);
            });
            services.AddHttpClient<IUserProxy, UserProxy>(httpClient =>
            {
                var serviceProvider = services.BuildServiceProvider();
                // Find the HttpContextAccessor service
                var httpContextAccessor = serviceProvider.GetService<IHttpContextAccessor>();
                // Get the bearer token from the request context (header)
                var bearerToken = httpContextAccessor.HttpContext.Request
                    .Headers["Authorization"]
                    .FirstOrDefault(h => h.StartsWith("bearer ", StringComparison.InvariantCultureIgnoreCase));
                // Add authorization if found
                if (bearerToken != null)
                    httpClient.DefaultRequestHeaders.Add("Authorization", bearerToken);
                //else
                //    httpClient.SetUserCertificateAuthorizationHeader(httpContextAccessor.HttpContext.User);
                httpClient.BaseAddress = new Uri(Configuration["UserManagementUrl"]);
            });
            services.AddSingleton<IAppointmentManagementService, AppointmentManagementService>();
            services.AddSingleton<IAppointmentProvider, AppointmentProvider>();
            services.AddSingleton(new AppointmentSettings(Configuration["AppointmentManagementUrl"]));
            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
                 .AddIdentityServerAuthentication(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.Authority = "https://localhost:22001/";
                    o.ApiName = "studioFly";
                })             
                .AddCookie()
                .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, o =>
                 {
                     o.Authority = "https://localhost:22001/";
                     o.RequireHttpsMetadata = false;
                     o.ClientId = "studioFly_code";
                     o.ClientSecret = "secret";
                    //o.ClientSecret = "K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=";
                    o.GetClaimsFromUserInfoEndpoint = true;
                     o.SaveTokens = true;
                     o.CorrelationCookie.Path = "/";
                     o.NonceCookie.Path = "/";
                     o.ResponseType = "code id_token";
                     o.Scope.Add("profile");
                     o.Scope.Add("openid");
                     o.Scope.Add("studioFlyApi");
                 });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddControllersAsServices();
            services.AddAuthorization();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseDefaultFiles();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc(routes => routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}"));
        }
    }
}
