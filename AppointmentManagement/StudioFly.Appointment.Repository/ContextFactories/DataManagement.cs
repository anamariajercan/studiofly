﻿using Microsoft.EntityFrameworkCore;
using StudioFly.Appointments.EntityFramework;

namespace StudioFly.Appointments.Repository.ContextFactories
{
    /// <summary>
    /// Manage the creation of db context
    /// </summary>
    public class DataManagement
    {
        /// <summary>
        /// Define the property of the db context
        /// </summary>
        public StudioFlyAppointmentContext StudioFlyAppointmentContext { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataManagement"/> class.
        /// </summary>
        public DataManagement()
        {
            StudioFlyAppointmentContext = new StudioFlyAppointmentContextFactory().CreateDbContext();
        }
        /// <summary>
        /// Commit data
        /// </summary>
        public void SaveData()
        {
            StudioFlyAppointmentContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Appointments ON");
            StudioFlyAppointmentContext.SaveChanges();
            StudioFlyAppointmentContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Appointments OFF");
        }

        /// <summary>
        /// Disposes the studio fly context
        /// </summary>
        ~DataManagement()
        {
            StudioFlyAppointmentContext.Dispose();
        }
    }
}
