﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StudioFly.Appointments.EntityFramework;

namespace StudioFly.Appointments.Repository.ContextFactories
{
    /// <summary>
    /// This class initialize the db connection
    /// </summary>
    public class StudioFlyAppointmentContextFactory : IDesignTimeDbContextFactory<StudioFlyAppointmentContext>
    {
        private static string _connectionString;

        /// <summary>
        /// initialize the db context
        /// </summary>
        /// <returns></returns>
        public StudioFlyAppointmentContext CreateDbContext()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            return CreateDbContext(args: null);
        }

        /// <summary>
        /// Creates the db context
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public StudioFlyAppointmentContext CreateDbContext(string[] args)
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                LoadConnectionString();
            }

            var builder = new DbContextOptionsBuilder<StudioFlyAppointmentContext>();
            builder.UseSqlServer(_connectionString);

            return new StudioFlyAppointmentContext(builder.Options);
        }

        /// <summary>
        /// Load the connection string from appSettings.json file
        /// </summary>
        private static void LoadConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            _connectionString = configuration.GetConnectionString("StudioFlyAppointment");
        }
    }
}
