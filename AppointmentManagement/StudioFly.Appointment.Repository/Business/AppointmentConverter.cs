﻿using System.Linq;
using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.Repository.Business
{
    internal class AppointmentConverter
    {
        readonly FeatureAppointmentType _featureAppointmentTypeManagement = new FeatureAppointmentType();
        readonly AppointmentsManagement _appointmentsManagementsManagement = new AppointmentsManagement();
        internal Appointment ToAppointmentModel(CreateAppointmentModel createAppModel)
        {
            Appointment appModel = new Appointment
            {
                ClientId = createAppModel.ClientId,
                EmployeeId = createAppModel.EmployeeId,
                FeatureAppointmentTypeId = createAppModel.FeatureAppointmentTypeId,
                Price = GetPriceForAppointment(createAppModel),
                AppointmentTime = createAppModel.AppointmentTime,
                OriginalId = createAppModel.OriginalId 
            };

            return appModel;
        }

        private int GetPriceForAppointment(CreateAppointmentModel createAppModel)
        {
            var feature = _appointmentsManagementsManagement.GetAllFeatureAppointmentTypes().FirstOrDefault(x=>x.Id==createAppModel.FeatureAppointmentTypeId);
            var price = feature.Price;
            if (createAppModel.AccessoriesId == null) return price;
            foreach (var accessoryId in createAppModel.AccessoriesId)
            {
                var accessory = _appointmentsManagementsManagement.GetAccessory(accessoryId);
                price += accessory.Price;
            }
            return price;
        }
    }
}
