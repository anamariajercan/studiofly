﻿using Microsoft.EntityFrameworkCore;
using StudioFly.Appointments.EntityFramework;
using StudioFly.Appointments.EntityFramework.Model;
using StudioFly.Appointments.Repository.ContextFactories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioFly.Appointments.Repository.Business
{
    /// <summary>
    /// Implementation of interface IAppointmentsManagement
    /// </summary>
    public class AppointmentsManagement : DataManagement, IAppointmentsManagement
    {
        /// <inheritdoc />
        /// <summary>
        /// Gets the appointments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Appointment GetAppointment(int id)
        {
            return StudioFlyAppointmentContext.Appointments.FirstOrDefault(appointment => appointment.Id == id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets all appointments.
        /// </summary>
        /// <returns></returns>
        public List<Appointment> GetAllAppointments()
        {
            return StudioFlyAppointmentContext.Appointments.ToList();
        }

        /// <summary>
        /// Gets all appointment types.
        /// </summary>
        /// <returns></returns>
        public List<AppointmentType> GetAllAppointmentTypes()
        {
            return StudioFlyAppointmentContext.AppointmentTypes.ToList();
        }

        /// <summary>
        /// Get an appointment type based on the id
        /// </summary>
        /// <param name="appointmentTypeName"></param>
        /// <returns></returns>
        public AppointmentType GetAppointmentType(string appointmentTypeName)
        {
            return StudioFlyAppointmentContext.AppointmentTypes.FirstOrDefault(appointment => appointment.Name == appointmentTypeName);
        }

        /// <summary>
        /// Creates the Appointment
        /// </summary>
        /// <param name="createAppModel"></param>
        /// <returns></returns>
        public Appointment CreateAppointment(CreateAppointmentModel createAppModel)
        {
            Appointment appModel;
            try
            {
                var appointmentConverter = new AppointmentConverter();
                appModel = appointmentConverter.ToAppointmentModel(createAppModel);
                StudioFlyAppointmentContext.Appointments.Add(appModel);
                SaveData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return GetAppointment(appModel.Id);
        }

        /// <summary>
        /// Updates an appointment
        /// </summary>
        /// <param name="createAppModel"></param>
        /// <returns></returns>
        public Appointment EditAppointment(CreateAppointmentModel createAppModel)
        {
            Appointment appModel = new Appointment();
            try
            {
                var appointmentConverter = new AppointmentConverter();
                appModel = appointmentConverter.ToAppointmentModel(createAppModel);
                StudioFlyAppointmentContext.Appointments.Update(appModel);
                SaveData();
                DeleteAppointment(appModel.OriginalId);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    if (entry.Entity is Appointment)
                    {
                        var proposedValues = entry.CurrentValues;
                        var databaseValues = entry.GetDatabaseValues();

                        foreach (var property in proposedValues.Properties)
                        {
                            var proposedValue = proposedValues[property];

                            proposedValues[property] = proposedValue;
                        }

                        // Refresh original values to bypass next concurrency check
                        entry.OriginalValues.SetValues(databaseValues);
                    }
                    else
                    {
                        throw new NotSupportedException(
                            "Don't know how to handle concurrency conflicts for "
                            + entry.Metadata.Name);
                    }
                }
            }
            return GetAppointment(appModel.Id);
        }

        /// <summary>
        /// Delete an appointment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteAppointment(int id)
        {
            try
            {
                var appModel = GetAppointment(id);
                StudioFlyAppointmentContext.Appointments.Remove(appModel);
                SaveData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }


        public IEnumerable<Feature> GetAllFeatures()
        {
            return StudioFlyAppointmentContext.Features;
        }

        public IEnumerable<Feature> GetFeatures(string appointmentTypeName)
        {
            var featureAppointmentTypes =
                StudioFlyAppointmentContext.FeatureAppointmentTypes.ToList();
            var features = new List<Feature>();
            foreach (var featureAppointmentType in featureAppointmentTypes)
            {
                var feature =
                    StudioFlyAppointmentContext.Features.FirstOrDefault(x => x.Id == featureAppointmentType.FeatureId);
                if (featureAppointmentType.AppointmentType.Name == appointmentTypeName)
                {
                    features.Add(feature);
                }
            }
            return features;
        }

        public IEnumerable<FeatureAppointmentType> GetAllFeatureAppointmentTypes()
        {
            var featureAppointmentTypes = StudioFlyAppointmentContext.FeatureAppointmentTypes.ToList();
            for (int i = 0; i < featureAppointmentTypes.Count; i++)
            {
                featureAppointmentTypes[i].AppointmentType = StudioFlyAppointmentContext.AppointmentTypes.ToList()
                    .FirstOrDefault(x => x.Id == featureAppointmentTypes[i].AppointmentTypeId);
                featureAppointmentTypes[i].Feature = StudioFlyAppointmentContext.Features.ToList()
                    .FirstOrDefault(x => x.Id == featureAppointmentTypes[i].FeatureId);
            }
            return featureAppointmentTypes;
        }

        /// <summary>
        /// Get an accessory based on the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Accessory GetAccessory(int id)
        {
            return StudioFlyAppointmentContext.Accessories.FirstOrDefault(accessory => accessory.Id == id);
        }
    }
}
