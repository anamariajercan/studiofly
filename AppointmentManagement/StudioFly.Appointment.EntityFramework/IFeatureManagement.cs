﻿using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.EntityFramework
{
    public interface IFeatureManagement
    {
        /// <summary>
        /// Get a specific feature from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Feature GetFeature(int id);
    }
}
