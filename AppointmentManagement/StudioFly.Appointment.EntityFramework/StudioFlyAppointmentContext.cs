﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.EntityFramework
{
    public class StudioFlyAppointmentContext : DbContext
    {
        public StudioFlyAppointmentContext() : base()
        {

        }
        public StudioFlyAppointmentContext(DbContextOptions<StudioFlyAppointmentContext> options) : base(options)
        {
        }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentType> AppointmentTypes { get; set; }
        public DbSet<AppointmentAccessory> AppointmentAccessories { get; set; }
        public DbSet<Accessory> Accessories { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<FeatureAppointmentType> FeatureAppointmentTypes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
