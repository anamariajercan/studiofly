﻿using System.Collections.Generic;
using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.EntityFramework
{
    /// <summary>
    /// Appointments management interface
    /// </summary>
    public interface IAppointmentsManagement
    {
        /// <summary>
        /// Gets the appointments.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Appointment GetAppointment(int id);
        /// <summary>
        /// Gets all appointments.
        /// </summary>
        /// <returns></returns>
        List<Appointment> GetAllAppointments();
        /// <summary>
        /// GetAllAppointmentsType
        /// </summary>
        /// <returns></returns>
        List<AppointmentType> GetAllAppointmentTypes();
        /// <summary>
        /// Get appointment type based on the id
        /// </summary>
        /// <param name="appointmentTypeName"></param>
        /// <returns></returns>
        AppointmentType GetAppointmentType(string appointmentTypeName);
       
        /// <summary>
        /// Create an appointment
        /// </summary>
        /// <param name="createAppModel"></param>
        /// <returns></returns>
        Appointment CreateAppointment(CreateAppointmentModel createAppModel);

        /// <summary>
        /// Updates an appointment
        /// </summary>
        /// <param name="appModel"></param>
        /// <returns></returns>
        Appointment EditAppointment(CreateAppointmentModel appModel);

        /// <summary>
        /// Delete an appointment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeleteAppointment(int id);

        /// <summary>
        /// Get All features
        /// </summary>
        /// <returns></returns>
        IEnumerable<Feature> GetAllFeatures();

        /// <summary>
        /// Gets the feature based on appointment type Name.
        /// </summary>
        /// <param name="appointmentTypeName">Name of the appointment type.</param>
        /// <returns></returns>
        IEnumerable<Feature> GetFeatures(string appointmentTypeName);

        /// <summary>
        /// Gets all feature appointment types.
        /// </summary>
        /// <returns></returns>
        IEnumerable<FeatureAppointmentType> GetAllFeatureAppointmentTypes();

        /// <summary>
        /// Gets the accessory.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Accessory GetAccessory(int id);
    }
}
