﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class FeatureAppointmentType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public AppointmentType AppointmentType { get; set; }
        public int AppointmentTypeId { get; set; }
        public Feature Feature { get; set; }
        public int FeatureId { get; set; }
        public int Price { get; set; }
        [Required]
        public int TimeToCompleteInMinutes { get; set; }
    }
}
