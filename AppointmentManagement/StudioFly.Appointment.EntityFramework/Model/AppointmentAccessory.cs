﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class AppointmentAccessory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        public int AccessoryId { get; set; }
        public Accessory Accessory { get; set; }
    }
}
