﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class Accessory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }
        public ICollection<AppointmentAccessory> AppointmentAccessories { get; set; }
    }
}
