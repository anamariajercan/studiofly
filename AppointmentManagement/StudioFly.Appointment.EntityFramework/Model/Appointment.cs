﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class Appointment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public DateTime AppointmentTime { get; set; }

        public int EmployeeId { get; set; }
        public int ClientId { get; set; }
        public int Price { get; set; }

        public int FeatureAppointmentTypeId { get; set; }
        public FeatureAppointmentType FeatureAppointmentType { get; set; }
        public ICollection<AppointmentAccessory> AppointmentAccessories { get; set; }
        public int OriginalId { get; set; }
    }
}
