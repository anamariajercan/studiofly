﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class AppointmentType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        
        public ICollection<FeatureAppointmentType> FeatureAppointmentTypes { get; set; }
    }
}
