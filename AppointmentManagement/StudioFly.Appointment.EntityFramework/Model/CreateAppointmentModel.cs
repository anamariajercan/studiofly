﻿using System;
using System.Collections.Generic;

namespace StudioFly.Appointments.EntityFramework.Model
{
    public class CreateAppointmentModel
    {
        public int Id { get; set; }
        public DateTime AppointmentTime { get; set; }

        public int EmployeeId { get; set; }
        public int ClientId { get; set; }
        public int FeatureAppointmentTypeId { get; set; }
        public List<int> AccessoriesId { get; set; }
        public int OriginalId { get; set; }
    }
}
