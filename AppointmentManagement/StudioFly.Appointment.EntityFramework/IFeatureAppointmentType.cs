﻿using System;
using System.Collections.Generic;
using System.Text;
using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.EntityFramework
{
    public interface IFeatureAppointmentType
    {
        List<FeatureAppointmentType> GetFeatureAppointmentTypes();
    }
}
