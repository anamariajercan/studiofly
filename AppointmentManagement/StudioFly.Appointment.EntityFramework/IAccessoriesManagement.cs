﻿using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointments.EntityFramework
{
    public interface IAccessoriesManagement
    {
        /// <summary>
        /// Get a specific accessory from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Accessory GetAccessory(int id);
    }
}
