﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using StudioFly.Appointments.EntityFramework;
using StudioFly.Appointments.Repository.Business;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StudioFly.Appointments.Services
{

    /// <summary>
    /// Setup class for services
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// configuration field
        /// </summary>
        public IConfiguration Configuration { get; }
        // transient - for every use, framework injects a new instance; multiple users can make paralel calls; single user can also make paralel calls
        // scoped - for every request, framework injects a new instance; single user cannot make paralel calls per request
        // singleton - framework creates an instance and reuse it for the lifetime of the server; one call at a time for all users
        // transient can use up all sockets if there are multiple users and multiple calls per user
        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            const string identityServiceUrl = "https://localhost:22001/";
            var xmlCommentsFile = $"{Assembly.GetEntryAssembly()?.GetName().Name}.xml";
            var xmlCommentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
            services.AddMvcCore(setupAction =>
                {
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
                    setupAction.Filters.Add(
                        new ProducesDefaultResponseTypeAttribute());
                    setupAction.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status401Unauthorized));

                    setupAction.Filters.Add(
                        new AuthorizeFilter());

                    setupAction.ReturnHttpNotAcceptable = true;
                    setupAction.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                })
                .AddApiExplorer()
                .AddAuthorization()
                .AddJsonFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddSingleton<IAppointmentsManagement, AppointmentsManagement>();
            services.AddApiVersioning(SetupApiVersionAction());
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, ConfigureJwtBearerOptions(identityServiceUrl));
            services.AddSwaggerGen(SetupSwaggerAction(xmlCommentsFullPath));
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseExceptionHandler(appBuilder => appBuilder.Run(async context =>
                {
                    // ensure generic 500 status code on fault.
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
                }));
            }
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
            app.UseMvcWithDefaultRoute();
            app.UseSwagger();
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/StudioFlyAppointmentManagement/swagger.json",
                    "Studio Fly Appointment Management API");
                setupAction.OAuthClientId("swaggerui");
                setupAction.OAuthRealm("test-realm");
                setupAction.OAuthAppName("studioFly");
                setupAction.OAuthScopeSeparator(" ");
                setupAction.OAuthUseBasicAuthenticationWithAccessCodeGrant();
            });
        }

        private static Action<ApiVersioningOptions> SetupApiVersionAction()
        {
            return o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            };
        }

        private static Action<SwaggerGenOptions> SetupSwaggerAction(string xmlCommentsFullPath)
        {
            return options =>
            {
                options.SwaggerDoc("StudioFlyAppointmentManagement",
                    new OpenApiInfo()
                    {
                        Title = "Studio Fly Appointment Management API",
                        Version = "1"
                    });
                options.IncludeXmlComments(xmlCommentsFullPath);
                options.OperationFilter<AddAuthHeaderOperationFilter>();
                var securitySchema = new OpenApiSecurityScheme
                {
                    Scheme = "oauth2",
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = new Uri("https://localhost:22001/connect/authorize"),
                            TokenUrl = new Uri("https://localhost:22001/connect/token"),
                            Scopes = new Dictionary<string, string>
                            {
                                {"studioFlyApi", "studioFlyApi"}
                            }
                        }
                    }
                };
                options.AddSecurityDefinition("oauth2", securitySchema);
            };
        }

        private static Action<JwtBearerOptions> ConfigureJwtBearerOptions(string identityServiceUrl)
        {
            return o =>
            {
                o.RequireHttpsMetadata = false;
                o.Authority = identityServiceUrl;
                o.Audience = "studioFlyApi";
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateLifetime = false,
                    ValidateAudience = false
                };
            };
        }
    }
    internal class AddAuthHeaderOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var isAuthorized = (context.MethodInfo.DeclaringType.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any()
                                || context.MethodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any())
                               && !context.MethodInfo.GetCustomAttributes(true).OfType<AllowAnonymousAttribute>().Any(); // this excludes methods with AllowAnonymous attribute

            if (!isAuthorized) return;

            operation.Responses.TryAdd("401", new OpenApiResponse { Description = "Unauthorized" });
            operation.Responses.TryAdd("403", new OpenApiResponse { Description = "Forbidden" });

            var jwtbearerScheme = new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
            };

            operation.Security = new List<OpenApiSecurityRequirement>
            {
                new OpenApiSecurityRequirement { [jwtbearerScheme] = new string []{} }
            };
        }
    }
}