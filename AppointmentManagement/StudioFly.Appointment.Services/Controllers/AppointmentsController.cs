﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using StudioFly.Appointments.EntityFramework.Model;
using StudioFly.Appointments.EntityFramework;

namespace StudioFly.Appointments.Services.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Authorize]
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AppointmentsController : ControllerBase
    {
        private readonly IAppointmentsManagement _appointmentsManagement;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        public AppointmentsController(IAppointmentsManagement appointmentsManagement, ILogger<AppointmentsController> logger)
        {
            _appointmentsManagement = appointmentsManagement;
            _logger = logger;
        }

        /// <summary>Create a new appointment.</summary>
        /// <param name="appModel"><see cref="Appointment">appointment details</see> to be created.</param>
        /// <response code="201">The newly created <see cref="Appointment">appointment model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="403">User does not have write permission.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("create/appointment")]
        [ProducesResponseType(typeof(Appointment), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Create([FromBody] CreateAppointmentModel appModel)
        {
            var created = _appointmentsManagement.CreateAppointment(appModel);
            return Created(new Uri($"/appointments/{created.Id}", UriKind.Relative), created);
        }

        /// <summary>Updates an existing appointment.</summary>
        /// <param name="appModel"><see cref="Appointment">appointment details</see> to be created.</param>
        /// <response code="201">The existing <see cref="Appointment">appointment model</see>.</response>
        /// <response code="400">Request parameters missing or invalid.</response>
        /// <response code="401">Unauthorized.</response>
        /// <response code="403">User does not have write permission.</response>
        /// <response code="404">Folder not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpPost("edit/appointment")]
        [ProducesResponseType(typeof(Appointment), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Edit([FromBody] CreateAppointmentModel appModel)
        {
            var updated = _appointmentsManagement.EditAppointment(appModel);
            return Ok(updated);
        }
        /// <summary>
        /// Deletes an existing appointment
        /// </summary>
        /// <param name="appointmentId">The appointment Id.</param>
        /// <response code="200">The appointment was successfully deleted.</response>
        /// <response code="400">The request is not valid</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="403">The request is understood, but it has been refused or access is not allowed.The user might not have access to the appointment.</response>
        /// <response code="404">The appointment was not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpDelete("delete/{appointmentId}")]
        [ProducesResponseType(typeof(Appointment), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult DeleteAppointment(int appointmentId)
        {
            try
            {
                _appointmentsManagement.DeleteAppointment(appointmentId);
                return Ok();
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAppointment failed for appointmentId {appointmentId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns an appointment based on the appointment id
        /// </summary>
        /// <returns>A <see cref="Appointment"/> object containing appointment content.</returns>
        /// <response code="200">The appointment was successfully returned.</response>
        /// <response code="400">The request is not valid</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="404">The appointment was not found.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("appointments/{id}")]
        [ProducesResponseType(typeof(Appointment), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppointment(int id)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointment = _appointmentsManagement.GetAppointment(id);
                return Ok(appointment);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAppointment failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns a list of appointments
        /// </summary>
        /// <returns>A <see cref="Appointment"/> object containing hello content.</returns>
        /// <response code="200">The appointment was successfully returned.</response>
        /// <response code="400">The request is not valid</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("appointments")]
        [ProducesResponseType(typeof(Appointment), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppointments()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointments = _appointmentsManagement.GetAllAppointments();
                return Ok(appointments);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAppointment failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns a list of appointments type
        /// </summary>
        /// <returns>A <see cref="AppointmentType"/> object containing hello content.</returns>
        /// <response code="200">The appointment type was successfully returned.</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("appointmentTypes")]
        [ProducesResponseType(typeof(AppointmentType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppointmentTypes()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointmentTypes = _appointmentsManagement.GetAllAppointmentTypes();
                return Ok(appointmentTypes);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAppointment failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns a list of feature appointments type
        /// </summary>
        /// <returns>A <see cref="FeatureAppointmentType"/> object containing hello content.</returns>
        /// <response code="200">The appointment type was successfully returned.</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("featureAppointmentTypes")]
        [ProducesResponseType(typeof(AppointmentType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetFeatureAppointmentTypes()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var featureAppointmentTypes = _appointmentsManagement.GetAllFeatureAppointmentTypes();
                return Ok(featureAppointmentTypes);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetFeatureAppointment failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns an appointment type based on its name
        /// </summary>
        /// <param name="appointmentTypeName">Appointment type name</param>
        /// <returns>A <see cref="AppointmentType"/> object containing appointment type content.</returns>
        /// <response code="200">The appointment type was successfully returned.</response>
        /// <response code="400">The request is not valid</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("appointmentTypes/appointmentTypeName")]
        [ProducesResponseType(typeof(AppointmentType), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppointmentTypes(string appointmentTypeName)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointmentType = _appointmentsManagement.GetAppointmentType(appointmentTypeName);
                return Ok(appointmentType);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAppointment failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAppointment failed");
            }
        }

        /// <summary>
        /// Endpoint that returns string a list of features
        /// </summary>
        /// <returns>A <see cref="Feature"/> object containing feature content.</returns>
        /// <response code="200">The features was successfully returned.</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("features")]
        [ProducesResponseType(typeof(Feature), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetFeatures()
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var appointments = _appointmentsManagement.GetAllFeatures();
                return Ok(appointments);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAllFeatures failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAllFeatures failed");
            }
        }

        /// <summary>
        /// Endpoint that returns a list of features
        /// </summary>
        /// <param name="appointmentTypeName"></param>
        /// <returns>A <see cref="Feature"/> object containing feature content.</returns>
        /// <response code="200">The features were successfully returned.</response>
        /// <response code="401">The request lacks valid authentication credentials.</response>
        /// <response code="500">Internal Server Error.</response>
        [HttpGet("features/{appointmentTypeName}")]
        [ProducesResponseType(typeof(Feature), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetFeatures(string appointmentTypeName)
        {
            string userId = string.Empty;
            try
            {
                userId = User.Identity.Name;
                ValidateModel();
                var features = _appointmentsManagement.GetFeatures(appointmentTypeName);
                return Ok(features);
            }
            catch (UnauthorizedAccessException ex)
            {
                return LogAndReturnUnauthorizedStatus(ex, $"GetAllFeatures failed for userid {userId}");
            }
            catch (ArgumentException ex)
            {
                return LogAndReturnBadRequestStatus(ex);
            }
            catch (Exception ex)
            {
                return LogAndReturnInternalServerErrorStatus(ex, "GetAllFeatures failed");
            }
        }

        private void ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid parameters");
            }
        }

        private IActionResult LogAndReturnUnauthorizedStatus(Exception ex, string message)
        {
            _logger.LogError(ex, message);
            return Unauthorized();
        }

        private IActionResult LogAndReturnBadRequestStatus(Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            ModelState.AddModelError("123", ex.Message);
            return BadRequest(ModelState);
        }

        private IActionResult LogAndReturnInternalServerErrorStatus(Exception exception, string errorMessage)
        {
            _logger.LogCritical(exception, $"{errorMessage}");
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}