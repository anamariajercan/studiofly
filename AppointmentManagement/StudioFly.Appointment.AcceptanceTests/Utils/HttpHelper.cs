﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StudioFly.Appointments.EntityFramework.Model;

namespace StudioFly.Appointment.AcceptanceTests.Utils
{
	/// <summary>
	///     Sends HTTP requests including an authorization header.
	/// </summary>
	public sealed class HttpHelper : IDisposable
	{
		private readonly string _apiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"];
		private readonly HttpClient _httpClient;
		private readonly ServiceResponseContext _responseContext;

		public HttpHelper(ServiceResponseContext responseContext)
		{
			_responseContext = responseContext;

			_httpClient = new HttpClient { BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiBaseAddress"]) };

			if (_apiBaseAddress.StartsWith("https", StringComparison.OrdinalIgnoreCase))
				ServicePointManager.SecurityProtocol =
					SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
		}

		public void Dispose()
		{
			_httpClient.Dispose();
		}

		public async Task<Appointments.EntityFramework.Model.Appointment> CreateAppointment(CreateAppointmentModel model, string userToken)
		{
			return await PostToService<Appointments.EntityFramework.Model.Appointment>("appointment/createAppointment", userToken, model).ConfigureAwait(false);
		}
        
        public async Task<Appointments.EntityFramework.Model.Appointment> GetAppointment(string appointmentId, string userToken)
        {
            return await GetFromService<Appointments.EntityFramework.Model.Appointment>($"appointment/{appointmentId}", userToken).ConfigureAwait(false);
        }

        private async Task<T> PostToService<T>(string apiEndpoint, string userToken, object model = null)
		{
			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userToken);

			HttpContent content = model != null
				? new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
				: null;
			return await PostToService<T>(apiEndpoint, content).ConfigureAwait(false);
		}

		private async Task<T> PostToService<T>(string apiEndpoint, HttpContent content)
		{
			var uri = GetUri(apiEndpoint);
			_responseContext.Response = await _httpClient.PostAsync(uri, content).ConfigureAwait(false);

			return await ReadContent<T>().ConfigureAwait(false);
		}

		private async Task<T> PutToService<T>(string apiEndpoint, string userToken, object model = null)
		{
			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userToken);

			HttpContent content = model != null
				? new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
				: null;
			return await PutToService<T>(apiEndpoint, content).ConfigureAwait(false);
		}

		private async Task<T> PutToService<T>(string apiEndpoint, HttpContent content)
		{
			var uri = GetUri(apiEndpoint);
			_responseContext.Response = await _httpClient.PutAsync(uri, content).ConfigureAwait(false);

			return await ReadContent<T>().ConfigureAwait(false);
		}

		private async Task<T> GetFromService<T>(string apiEndpoint, string userToken)
		{
			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userToken);

			var uri = GetUri(apiEndpoint);
			_responseContext.Response = await _httpClient.GetAsync(uri).ConfigureAwait(false);

			return await ReadContent<T>().ConfigureAwait(false);
		}

		private Uri GetUri(string apiUri)
		{
			if (string.IsNullOrEmpty(apiUri))
				throw new ArgumentException("Invalid endpoint", nameof(apiUri));

			return new Uri(_apiBaseAddress + apiUri);
		}

		private async Task<T> ReadContent<T>()
		{
			if (!_responseContext.IsSuccessful) return default(T);

			var contentStream = await _responseContext.Content.ReadAsStreamAsync().ConfigureAwait(false);
			using (var reader = new StreamReader(contentStream))
			{
				var json = reader.ReadToEnd();
				return JsonConvert.DeserializeObject<T>(json);
			}
		}
	}
}