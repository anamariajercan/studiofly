﻿using System.Collections.Generic;

namespace StudioFly.Appointment.AcceptanceTests.Utils
{
	/// <summary>
	/// Class used to share the properties needed between steps
	/// </summary>
	public class StudioFlyContext
	{
        public StudioFlyContext()
        {
            ItemsToDelete = new List<string>();
        }

        public List<string> ItemsToDelete { get;}

        public Appointments.EntityFramework.Model.Appointment Appointment { get; set; }

	}
}
