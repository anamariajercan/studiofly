﻿namespace StudioFly.Appointment.AcceptanceTests.Utils
{
    public class UserTokenContext
    {
        /// <summary>
        ///     Admin user token
        /// </summary>
        public string AccessToken { get; set; }
	}
}