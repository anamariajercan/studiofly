﻿//************************************************************************************************
// Copyright © 2019 Waters Corporation. All Rights Reserved.
//
//************************************************************************************************

using System;
using System.Threading.Tasks;
using StudioFly.Appointment.AcceptanceTests.Utils;
using StudioFly.Appointments.EntityFramework.Model;
using TechTalk.SpecFlow;

namespace StudioFly.Appointment.AcceptanceTests.Steps
{
    [Binding]
    public sealed class CreateSteps
    {
        private readonly UserTokenContext _userTokenContext;
        private readonly StudioFlyContext _studioFlyContext;
        private readonly HttpHelper _httpHelper;

        public CreateSteps(UserTokenContext userTokenContext,
            StudioFlyContext studioFlyContext, HttpHelper httpHelper)
        {
            _userTokenContext = userTokenContext;
            _studioFlyContext = studioFlyContext;
            _httpHelper = httpHelper;
        }

        [When(@"an appointment creation is requested")]
        public async Task WhenAnAppointmentCreationIsRequested()
        {
            var appModel = InitiateCreateAppointmentModel();
            var appointment = await _httpHelper.CreateAppointment(appModel, _userTokenContext.AccessToken).ConfigureAwait(true);
            if (appointment != null)
            {
                _studioFlyContext.Appointment = appointment;
                _studioFlyContext.ItemsToDelete.Add(appointment.Id.ToString());
            }
        }

        private CreateAppointmentModel InitiateCreateAppointmentModel()
        {
            var appointment = new CreateAppointmentModel
            {
                FeatureAppointmentTypeId = 1,
                ClientId = 1,
                EmployeeId = 1,
                AppointmentTime = DateTime.Now
            };

            return appointment;
        }
    }
}
