﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudioFly.Appointment.AcceptanceTests.Utils;
using TechTalk.SpecFlow;

namespace StudioFly.Appointment.AcceptanceTests.Steps
{
	[Binding]
	public class CommonSteps
	{
		private readonly ServiceResponseContext _responseContext;
        private readonly StudioFlyContext _studioFlyContext;


        public CommonSteps(
			ServiceResponseContext responseContext, StudioFlyContext studioFlyContext)
		{
			_responseContext = responseContext;
            _studioFlyContext = studioFlyContext;
        }

        [Then(@"the service shall return status code (\d*)")]
        public void CheckStatusCode(int expectedStatusCode)
        {
            var actualStatusCode = (int) _responseContext.StatusCode;
            Assert.AreEqual(expectedStatusCode, actualStatusCode,
                $"Check the expected status code {expectedStatusCode} is equal with actual status code {actualStatusCode}");
        }

        [Then(@"an appointment is created")]
        public void ThenAnAppointmentIsCreated()
        {
           Assert.IsNotNull(_studioFlyContext.Appointment);
        }

    }
}