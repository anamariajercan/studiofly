SET IDENTITY_INSERT [dbo].[Features] ON
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (1, N'Clasic')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (2, N'OjaSemipermanenta')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (3, N'Gentelman')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (4, N'Indepartare')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (5, N'Gel')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (6, N'Complex')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (7, N'ConstructieGel')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (8, N'Reumplere')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (9, N'ConstructieFortaExtrema')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (10, N'ConstructieSuperSlimNails')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (11, N'Reparat')
INSERT INTO [dbo].[Features] ([Id], [Name]) VALUES (12, N'RetusDeCuloare')
SET IDENTITY_INSERT [dbo].[Features] OFF
