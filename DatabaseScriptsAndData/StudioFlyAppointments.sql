USE [master]
GO
/****** Object:  Database [StudioFlyAppointments]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE DATABASE [StudioFlyAppointments]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'StudioFlyAppointments', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\StudioFlyAppointments.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'StudioFlyAppointments_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\StudioFlyAppointments_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [StudioFlyAppointments] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StudioFlyAppointments].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StudioFlyAppointments] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET ARITHABORT OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [StudioFlyAppointments] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StudioFlyAppointments] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StudioFlyAppointments] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET  ENABLE_BROKER 
GO
ALTER DATABASE [StudioFlyAppointments] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StudioFlyAppointments] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [StudioFlyAppointments] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [StudioFlyAppointments] SET  MULTI_USER 
GO
ALTER DATABASE [StudioFlyAppointments] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StudioFlyAppointments] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StudioFlyAppointments] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StudioFlyAppointments] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [StudioFlyAppointments] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [StudioFlyAppointments] SET QUERY_STORE = OFF
GO
USE [StudioFlyAppointments]
GO
/****** Object:  Table [dbo].[Accessories]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accessories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Accessories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentAccessories]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentAccessories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[AccessoryId] [int] NOT NULL,
 CONSTRAINT [PK_AppointmentAccessories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Appointments]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appointments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentTime] [datetime2](7) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[Price] [int] NOT NULL,
	[FeatureAppointmentTypeId] [int] NOT NULL,
	[OriginalId] [int] NOT NULL,
 CONSTRAINT [PK_Appointments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentTypes]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_AppointmentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeatureAppointmentTypes]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeatureAppointmentTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentTypeId] [int] NOT NULL,
	[FeatureId] [int] NOT NULL,
	[Price] [int] NOT NULL,
	[TimeToCompleteInMinutes] [int] NOT NULL,
 CONSTRAINT [PK_FeatureAppointmentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Features]    Script Date: 6/23/2020 12:32:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Features](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Features] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_AppointmentAccessories_AccessoryId]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE NONCLUSTERED INDEX [IX_AppointmentAccessories_AccessoryId] ON [dbo].[AppointmentAccessories]
(
	[AccessoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AppointmentAccessories_AppointmentId]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE NONCLUSTERED INDEX [IX_AppointmentAccessories_AppointmentId] ON [dbo].[AppointmentAccessories]
(
	[AppointmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Appointments_FeatureAppointmentTypeId]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE NONCLUSTERED INDEX [IX_Appointments_FeatureAppointmentTypeId] ON [dbo].[Appointments]
(
	[FeatureAppointmentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FeatureAppointmentTypes_AppointmentTypeId]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE NONCLUSTERED INDEX [IX_FeatureAppointmentTypes_AppointmentTypeId] ON [dbo].[FeatureAppointmentTypes]
(
	[AppointmentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FeatureAppointmentTypes_FeatureId]    Script Date: 6/23/2020 12:32:39 PM ******/
CREATE NONCLUSTERED INDEX [IX_FeatureAppointmentTypes_FeatureId] ON [dbo].[FeatureAppointmentTypes]
(
	[FeatureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AppointmentAccessories]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentAccessories_Accessories_AccessoryId] FOREIGN KEY([AccessoryId])
REFERENCES [dbo].[Accessories] ([Id])
GO
ALTER TABLE [dbo].[AppointmentAccessories] CHECK CONSTRAINT [FK_AppointmentAccessories_Accessories_AccessoryId]
GO
ALTER TABLE [dbo].[AppointmentAccessories]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentAccessories_Appointments_AppointmentId] FOREIGN KEY([AppointmentId])
REFERENCES [dbo].[Appointments] ([Id])
GO
ALTER TABLE [dbo].[AppointmentAccessories] CHECK CONSTRAINT [FK_AppointmentAccessories_Appointments_AppointmentId]
GO
ALTER TABLE [dbo].[Appointments]  WITH CHECK ADD  CONSTRAINT [FK_Appointments_FeatureAppointmentTypes_FeatureAppointmentTypeId] FOREIGN KEY([FeatureAppointmentTypeId])
REFERENCES [dbo].[FeatureAppointmentTypes] ([Id])
GO
ALTER TABLE [dbo].[Appointments] CHECK CONSTRAINT [FK_Appointments_FeatureAppointmentTypes_FeatureAppointmentTypeId]
GO
ALTER TABLE [dbo].[FeatureAppointmentTypes]  WITH CHECK ADD  CONSTRAINT [FK_FeatureAppointmentTypes_AppointmentTypes_AppointmentTypeId] FOREIGN KEY([AppointmentTypeId])
REFERENCES [dbo].[AppointmentTypes] ([Id])
GO
ALTER TABLE [dbo].[FeatureAppointmentTypes] CHECK CONSTRAINT [FK_FeatureAppointmentTypes_AppointmentTypes_AppointmentTypeId]
GO
ALTER TABLE [dbo].[FeatureAppointmentTypes]  WITH CHECK ADD  CONSTRAINT [FK_FeatureAppointmentTypes_Features_FeatureId] FOREIGN KEY([FeatureId])
REFERENCES [dbo].[Features] ([Id])
GO
ALTER TABLE [dbo].[FeatureAppointmentTypes] CHECK CONSTRAINT [FK_FeatureAppointmentTypes_Features_FeatureId]
GO
USE [master]
GO
ALTER DATABASE [StudioFlyAppointments] SET  READ_WRITE 
GO
