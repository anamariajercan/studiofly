SET IDENTITY_INSERT [dbo].[Accessories] ON
INSERT INTO [dbo].[Accessories] ([Id], [Name], [Price]) VALUES (1, N'Piatra Swarowsky', 5)
INSERT INTO [dbo].[Accessories] ([Id], [Name], [Price]) VALUES (2, N'Bijuterie pe unghie', 25)
INSERT INTO [dbo].[Accessories] ([Id], [Name], [Price]) VALUES (3, N'Slider', 5)
SET IDENTITY_INSERT [dbo].[Accessories] OFF
