SET IDENTITY_INSERT [dbo].[Appointments] ON
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (3, N'2020-05-24 09:00:00', 1, 1, 50, 1, 0)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (4, N'2020-05-23 11:18:00', 1, 1, 50, 1, 2)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (5, N'2020-05-26 07:00:00', 1, 1, 50, 1, 0)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (6, N'2020-05-28 10:00:00', 2, 2, 80, 3, 0)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (7, N'2020-05-29 07:00:00', 3, 1, 100, 4, 0)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (8, N'2020-06-03 07:30:00', 1, 1, 50, 1, 0)
INSERT INTO [dbo].[Appointments] ([Id], [AppointmentTime], [EmployeeId], [ClientId], [Price], [FeatureAppointmentTypeId], [OriginalId]) VALUES (9, N'2020-06-24 09:00:00', 1, 1, 50, 1, 0)
SET IDENTITY_INSERT [dbo].[Appointments] OFF
