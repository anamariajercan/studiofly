SET IDENTITY_INSERT [dbo].[AppointmentTypes] ON
INSERT INTO [dbo].[AppointmentTypes] ([Id], [Name]) VALUES (1, N'Manichiura')
INSERT INTO [dbo].[AppointmentTypes] ([Id], [Name]) VALUES (2, N'Pedichiura')
INSERT INTO [dbo].[AppointmentTypes] ([Id], [Name]) VALUES (3, N'ManichiuraTehnica')
SET IDENTITY_INSERT [dbo].[AppointmentTypes] OFF
